package com.colt.novitas;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class EnvironmentProperties implements Serializable {

	private static final long serialVersionUID = -1744774938351026878L;

	private String smtpHost;
	private String smtpPort;
	private String smtpUserName;
	private String smtpPassword;
	private Boolean smtpStarTls;

	private String configServiceUrl;
	private String environment;
	private String requestAPIUrl;
	private String requestAPIUsername;
	private String requestAPIPassword;
	private String serviceAPIBaseUrl;
	private String serviceAPIUsername;
	private String serviceAPIPassword;
	private String billingAPIOneOffUrl;
	private String billingAPIRecurrUrl;
	private String billingAPIUsername;
	private String billingAPIPassword;
	private boolean mockBlueplanet;
	private String intnalJBPMUrl;
	private String intnalJBPMUsername;
	private String intnalJBPMPassword;
	private int maxIterations;
	private String callBackUrl;
	private String siebelUrl;
	private String siebelUsername;
	private String siebelPassword;
	private String smartSystemUrl;
	private String smartSystemUsername;
	private String smartSystemPassword;
	private String xngPortAPIUrl;
	private String xngPortAPIUsername;
	private String xngPortAPIPassword;
	private String xngConnectionAPIUrl;
	private String xngConnectionAPIUsername;
	private String xngConnectionAPIPassword;
	private String premiseMasterLocationAPIUrl;
	private String premiseMasterLocationAPIUsername;
	private String premiseMasterLocationAPIPassword;
	private String kenanInterfaceUrl;
	private String kenanInterfaceUsername;
	private String kenanInterfacePassword;
	private String portDeploymentId;
	private String connectionDeploymentId;
	private String onboardingDeploymentId;
	private String inboundRestApiUrl;
	private String inboundRestApiUserName;
	private String inboundRestApiPassword;
	private String adminApiLoginUrl;
	private String adminApiCustomerUrl;
	private String adminApiUserName;
	private String adminApiPassword;
	private String azureApiUrl;
	private String azureJksPath;
	private String azureJksPassword;
	private String azureSubscriptionId;
	private String azureApiVersionId;
	private int azureRequestTimeOut;
	private String internalDeploymentId;
	private String customerCeaseOrRestoreDeploymentId;
	private String internalCustomerDeploymentId;
	private String ncInternalCustomerDeploymentId;
	private String dcaPortDeploymentId;
	private String dcaConnectionDeploymentId;
	private String sitePortDeploymentId;
	private String emailFromAddress;
	private String emailToAddress;
	private String asiaToEmail;
	private String ccEmail;
	private String bccEmail;
	private String sitePortAdminUiLink;
	private String jbpmUrl;
	private String awshCreatePortDepId;
	private String awshDeletePortDepId;
	private String awshCreateConnDepId;
	private String awshModifyConnDepId;
	private String awshDeleteConnDepId;
	private String awsdCreatePortDepId;
	private String awsdDeletePortDepId;
	private String awsdCreateConnDepId;
	private String awsdModifyConnDepId;
	private String awsdDeleteConnDepId;
	private String awsAccessKey;
	private String awsSecretKey;
	private String awsAsiaAccessKey;
	private String awsAsiaSecretKey;
	private String awsEndPoint;
	private String pollerDelay;
	private String deletePollerDelay;
	private boolean isAWSMocked;
	private boolean isXngMocked;
	private String poAsyncUrl;
	private String poVersion;
	private String scheduleApiUrl;
	private String scheduleApiUserName;
	private String scheduleApiPassword;
	private String scheduleApiVersion;
	private String stdCreateConnDepId;
	private String azureCreateConnDepId;
	private String stdModifyConnDepId;
	private String azureModifyConnDepId;
	private String stdDeleteConnDepId;
	private String azureDeleteConnDepId;
	private int awsConnectionTimeOut;
	private Integer mdsoRecoveryMaxRetries;
	private String addressBookPath;
	private String tempLocationPath;
	private String customerAPIUrl;
	private String customerAPIUserName;
	private String customerAPIPassword;
	private String onboardingEmailToAddress;
	private String asiaOnboardingDepId;
	private String verizonCircuitId;
	private String verizonBillingId;
	private String mdsoAzureServiceProfile;
	private String mdsoDefaultServiceProfile;
	private String poAzureServiceProfile;
	private String poDefaultServiceProfile;
	private String ncBaseUrl;
	private String ncUserName;
	private String ncPassword;
	private String ncStdModifyConnDepId;
	private String ncAzureModifyConnDepId;
	private String ncStdDeleteConnDepId;
	private String ncAzureDeleteConnDepId;
	private String ncAwshModifyConnDepId;
	private String ncAwshDeleteConnDepId;
	private String ncAwsdModifyConnDepId;
	private String ncAwsdDeleteConnDepId;
	private String xcCreateDepId;
	private String xcDeleteDepId;
	private String xcDelay;
	private String mefUrl;
	private Integer requestId;
	private String systemStatusUrl;
	private String systemStatusUserName;
	private String systemStatusPassword;
	private String moduleStatusPollerTime;
	private String stdDeletePortDepId;
	private String azureDeletePortDepId;
	private String mefStdCreateConnDepId;
	private String mefAwshCreatConnDeId;
	private String locationApiUrl;
	private String logUniqueId;
	private String portOrConnId;
	private String errorToEmail;
	private String resourceNewApiUrl;
	private String gcpBaseUrl;
	private String gcpCreatePortDepId;
	private String gcpDeletePortDepId;
	private String gcpCreateConnDepId;
	private String gcpModifyConnDepId;
	private String gcpDeleteConnDepId;

	private String ibmBaseUrl;
	private String ibmCreatePortDepId;
	private String ibmDeletePortDepId;
	private String ibmCreateConnDepId;
	private String ibmModifyConnDepId;
	private String ibmDeleteConnDepId;

	private String atosToEmail;
	private String atosConnDepId;

	private String ipaccessAPIBaseUrl;
	private String ipaccessAPIUsername;
	private String ipaccessAPIPassword;

	private String eipAPIBaseUrl;
	private String eipAPIUsername;
	private String eipAPIPassword;

	private String juniperSARAPIBaseUrl;
	private String juniperSARAPIUsername;
	private String juniperSARAPIPassword;

	private String ipaccessCreateConnDepId;
	private String ipaccessModifyConnDepId;
	private String ipaccessDeleteConnDepId;
	private String xngIPACSPName;
	private String xngBaseUrl;
	private String xngUsername;
	private String xngPassword;
	private String ipaccessNNIVLANMapping;
	private String ipaccessNNIVLANType;
	private String juniperSARDeviceCredential;

	private String siebelServiceBaseUrl;
	private String siebelServiceUsername;
	private String siebelServicePassword;
	private String sparkCapacityToEmail;

	private String requestDependencyBaseUrl;
	
	private String oracleBaseUrl;
	private String oracleCreatePortDepId;
	private String oracleDeletePortDepId;
	private String oracleCreateConnDepId;
	private String oracleModifyConnDepId;
	private String oracleDeleteConnDepId;
	
	// Delete standard port email notification to SD team
	private String deviceRemovalEmailTo;
	private String deviceRemovalEmailCc;
	private String deviceRemovalEmailFrom;
	private String deviceRemovalEmailCountries;
	
	//ticketing
	private String ticketNotificationEmailTo;
	
	//planned work notification
	private String plannedWorksToEmail;
	private String plannedWorksCCEmail;

	private String equinixBaseUrl;
	private String equinixCreatePortDepId;
	private String equinixDeletePortDepId;
	private String equinixCreateConnDepId;
	private String equinixModifyConnDepId;
	private String equinixDeleteConnDepId;

	public String getDeviceRemovalEmailCountries() {
		return deviceRemovalEmailCountries;
	}
	
	public void setDeviceRemovalEmailCountries(String deviceRemovalEmailCountries) {
		this.deviceRemovalEmailCountries = deviceRemovalEmailCountries;
	}
	
	public String getDeviceRemovalEmailCc() {
		return deviceRemovalEmailCc;
	}
	
	public String getDeviceRemovalEmailFrom() {
		return deviceRemovalEmailFrom;
	}
	
	public String getDeviceRemovalEmailTo() {
		return deviceRemovalEmailTo;
	}
	
	public void setDeviceRemovalEmailCc(String deviceRemovalEmailCc) {
		this.deviceRemovalEmailCc = deviceRemovalEmailCc;
	}
	
	public void setDeviceRemovalEmailFrom(String deviceRemovalEmailFrom) {
		this.deviceRemovalEmailFrom = deviceRemovalEmailFrom;
	}
	
	public void setDeviceRemovalEmailTo(String deviceRemovalEmailTo) {
		this.deviceRemovalEmailTo = deviceRemovalEmailTo;
	}
	
	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUserName() {
		return smtpUserName;
	}

	public void setSmtpUserName(String smtpUserName) {
		this.smtpUserName = smtpUserName;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public Boolean getSmtpStarTls() {
		return smtpStarTls;
	}

	public void setSmtpStarTls(Boolean smtpStarTls) {
		this.smtpStarTls = smtpStarTls;
	}

	public String getSparkCapacityToEmail() {
		return sparkCapacityToEmail;
	}
	
	public void setSparkCapacityToEmail(String sparkCapacityToEmail) {
		this.sparkCapacityToEmail = sparkCapacityToEmail;
	}

	public String getSiebelServiceBaseUrl() {
		return siebelServiceBaseUrl;
	}

	public void setSiebelServiceBaseUrl(String siebelServiceBaseUrl) {
		this.siebelServiceBaseUrl = siebelServiceBaseUrl;
	}

	public String getSiebelServicePassword() {
		return siebelServicePassword;
	}

	public void setSiebelServicePassword(String siebelServicePassword) {
		this.siebelServicePassword = siebelServicePassword;
	}

	public String getSiebelServiceUsername() {
		return siebelServiceUsername;
	}

	public void setSiebelServiceUsername(String siebelServiceUsername) {
		this.siebelServiceUsername = siebelServiceUsername;
	}

	public String getJuniperSARDeviceCredential() {
		return juniperSARDeviceCredential;
	}

	public void setJuniperSARDeviceCredential(String juniperSARDeviceCredential) {
		this.juniperSARDeviceCredential = juniperSARDeviceCredential;
	}

	public String getIpaccessNNIVLANMapping() {
		return ipaccessNNIVLANMapping;
	}

	public void setIpaccessNNIVLANMapping(String ipaccessNNIVLANMapping) {
		this.ipaccessNNIVLANMapping = ipaccessNNIVLANMapping;
	}

	public String getIpaccessNNIVLANType() {
		return ipaccessNNIVLANType;
	}

	public void setIpaccessNNIVLANType(String ipaccessNNIVLANType) {
		this.ipaccessNNIVLANType = ipaccessNNIVLANType;
	}

	public String getXngBaseUrl() {
		return xngBaseUrl;
	}

	public void setXngBaseUrl(String xngBaseUrl) {
		this.xngBaseUrl = xngBaseUrl;
	}

	public String getXngUsername() {
		return xngUsername;
	}

	public void setXngUsername(String xngUsername) {
		this.xngUsername = xngUsername;
	}

	public String getXngPassword() {
		return xngPassword;
	}

	public void setXngPassword(String xngPassword) {
		this.xngPassword = xngPassword;
	}

	public String getXngIPACSPName() {
		return xngIPACSPName;
	}

	public void setXngIPACSPName(String xngIPACSPName) {
		this.xngIPACSPName = xngIPACSPName;
	}

	public String getEipAPIBaseUrl() {
		return eipAPIBaseUrl;
	}

	public void setEipAPIBaseUrl(String eipAPIBaseUrl) {
		this.eipAPIBaseUrl = eipAPIBaseUrl;
	}

	public String getEipAPIUsername() {
		return eipAPIUsername;
	}

	public void setEipAPIUsername(String eipAPIUsername) {
		this.eipAPIUsername = eipAPIUsername;
	}

	public String getEipAPIPassword() {
		return eipAPIPassword;
	}

	public void setEipAPIPassword(String eipAPIPassword) {
		this.eipAPIPassword = eipAPIPassword;
	}

	public String getIpaccessAPIBaseUrl() {
		return ipaccessAPIBaseUrl;
	}

	public void setIpaccessAPIBaseUrl(String ipaccessAPIBaseUrl) {
		this.ipaccessAPIBaseUrl = ipaccessAPIBaseUrl;
	}

	public String getJuniperSARAPIBaseUrl() {
		return juniperSARAPIBaseUrl;
	}

	public void setJuniperSARAPIBaseUrl(String juniperSARAPIBaseUrl) {
		this.juniperSARAPIBaseUrl = juniperSARAPIBaseUrl;
	}

	public String getJuniperSARAPIUsername() {
		return juniperSARAPIUsername;
	}

	public void setJuniperSARAPIUsername(String juniperSARAPIUsername) {
		this.juniperSARAPIUsername = juniperSARAPIUsername;
	}

	public String getJuniperSARAPIPassword() {
		return juniperSARAPIPassword;
	}

	public void setJuniperSARAPIPassword(String juniperSARAPIPassword) {
		this.juniperSARAPIPassword = juniperSARAPIPassword;
	}

	public String getIpaccessAPIUsername() {
		return ipaccessAPIUsername;
	}

	public void setIpaccessAPIUsername(String ipaccessAPIUsername) {
		this.ipaccessAPIUsername = ipaccessAPIUsername;
	}

	public String getIpaccessAPIPassword() {
		return ipaccessAPIPassword;
	}

	public void setIpaccessAPIPassword(String ipaccessAPIPassword) {
		this.ipaccessAPIPassword = ipaccessAPIPassword;
	}

	public String getIpaccessCreateConnDepId() {
		return ipaccessCreateConnDepId;
	}

	public void setIpaccessCreateConnDepId(String ipaccessCreateConnDepId) {
		this.ipaccessCreateConnDepId = ipaccessCreateConnDepId;
	}

	public String getIpaccessModifyConnDepId() {
		return ipaccessModifyConnDepId;
	}

	public void setIpaccessModifyConnDepId(String ipaccessModifyConnDepId) {
		this.ipaccessModifyConnDepId = ipaccessModifyConnDepId;
	}

	public String getIpaccessDeleteConnDepId() {
		return ipaccessDeleteConnDepId;
	}

	public void setIpaccessDeleteConnDepId(String ipaccessDeleteConnDepId) {
		this.ipaccessDeleteConnDepId = ipaccessDeleteConnDepId;
	}

	public String getAtosConnDepId() {
		return atosConnDepId;
	}

	public void setAtosConnDepId(String atosConnDepId) {
		this.atosConnDepId = atosConnDepId;
	}

	public String getAtosToEmail() {
		return atosToEmail;
	}

	public void setAtosToEmail(String atosToEmail) {
		this.atosToEmail = atosToEmail;
	}

	public String getGcpCreatePortDepId() {
		return gcpCreatePortDepId;
	}

	public void setGcpCreatePortDepId(String gcpCreatePortDepId) {
		this.gcpCreatePortDepId = gcpCreatePortDepId;
	}

	public String getGcpDeletePortDepId() {
		return gcpDeletePortDepId;
	}

	public void setGcpDeletePortDepId(String gcpDeletePortDepId) {
		this.gcpDeletePortDepId = gcpDeletePortDepId;
	}

	public String getGcpCreateConnDepId() {
		return gcpCreateConnDepId;
	}

	public void setGcpCreateConnDepId(String gcpCreateConnDepId) {
		this.gcpCreateConnDepId = gcpCreateConnDepId;
	}

	public String getGcpModifyConnDepId() {
		return gcpModifyConnDepId;
	}

	public void setGcpModifyConnDepId(String gcpModifyConnDepId) {
		this.gcpModifyConnDepId = gcpModifyConnDepId;
	}

	public String getGcpDeleteConnDepId() {
		return gcpDeleteConnDepId;
	}

	public void setGcpDeleteConnDepId(String gcpDeleteConnDepId) {
		this.gcpDeleteConnDepId = gcpDeleteConnDepId;
	}

	public String getGcpBaseUrl() {
		return gcpBaseUrl;
	}

	public void setGcpBaseUrl(String gcpBaseUrl) {
		this.gcpBaseUrl = gcpBaseUrl;
	}

	public String getResourceNewApiUrl() {
		return resourceNewApiUrl;
	}

	public void setResourceNewApiUrl(String resourceNewApiUrl) {
		this.resourceNewApiUrl = resourceNewApiUrl;
	}

	public String getErrorToEmail() {
		return errorToEmail;
	}

	public void setErrorToEmail(String errorToEmail) {
		this.errorToEmail = errorToEmail;
	}

	public Integer getMdsoRecoveryMaxRetries() {
		return mdsoRecoveryMaxRetries;
	}

	public void setMdsoRecoveryMaxRetries(Integer mdsoRecoveryMaxRetries) {
		this.mdsoRecoveryMaxRetries = mdsoRecoveryMaxRetries;
	}

	public boolean isAWSMocked() {
		return isAWSMocked;
	}

	public void setAWSMocked(boolean isAWSMocked) {
		this.isAWSMocked = isAWSMocked;
	}

	public String getPollerDelay() {

		return pollerDelay;
	}

	public String getAzureDeleteConnDepId() {
		return azureDeleteConnDepId;
	}

	public void setAzureDeleteConnDepId(String azureDeleteConnDepId) {
		this.azureDeleteConnDepId = azureDeleteConnDepId;
	}

	public void setPollerDelay(String pollerDelay) {
		this.pollerDelay = pollerDelay;
	}

	public String getAwsEndPoint() {
		return awsEndPoint;
	}

	public void setAwsEndPoint(String awsEndPoint) {
		this.awsEndPoint = awsEndPoint;
	}

	public String getAwsAccessKey() {
		return awsAccessKey;
	}

	public void setAwsAccessKey(String awsAccessKey) {
		this.awsAccessKey = awsAccessKey;
	}

	public String getAwsSecretKey() {
		return awsSecretKey;
	}

	public void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}

	public String getJbpmUrl() {
		return jbpmUrl;
	}

	public void setJbpmUrl(String jbpmUrl) {
		this.jbpmUrl = jbpmUrl;
	}

	public String getAwshCreatePortDepId() {
		return awshCreatePortDepId;
	}

	public void setAwshCreatePortDepId(String awshCreatePortDepId) {
		this.awshCreatePortDepId = awshCreatePortDepId;
	}

	public String getAwshDeletePortDepId() {
		return awshDeletePortDepId;
	}

	public void setAwshDeletePortDepId(String awshDeletePortDepId) {
		this.awshDeletePortDepId = awshDeletePortDepId;
	}

	public String getAwshCreateConnDepId() {
		return awshCreateConnDepId;
	}

	public void setAwshCreateConnDepId(String awshCreateConnDepId) {
		this.awshCreateConnDepId = awshCreateConnDepId;
	}

	public String getAwshModifyConnDepId() {
		return awshModifyConnDepId;
	}

	public void setAwshModifyConnDepId(String awshModifyConnDepId) {
		this.awshModifyConnDepId = awshModifyConnDepId;
	}

	public String getAwshDeleteConnDepId() {
		return awshDeleteConnDepId;
	}

	public void setAwshDeleteConnDepId(String awshDeleteConnDepId) {
		this.awshDeleteConnDepId = awshDeleteConnDepId;
	}

	public String getXngConnectionAPIUrl() {
		return xngConnectionAPIUrl;
	}

	public void setXngConnectionAPIUrl(String xngConnectionAPIUrl) {
		this.xngConnectionAPIUrl = xngConnectionAPIUrl;
	}

	public String getXngConnectionAPIUsername() {
		return xngConnectionAPIUsername;
	}

	public void setXngConnectionAPIUsername(String xngConnectionAPIUsername) {
		this.xngConnectionAPIUsername = xngConnectionAPIUsername;
	}

	public String getXngConnectionAPIPassword() {
		return xngConnectionAPIPassword;
	}

	public void setXngConnectionAPIPassword(String xngConnectionAPIPassword) {
		this.xngConnectionAPIPassword = xngConnectionAPIPassword;
	}

	public String getXngResourceAPIUrl() {
		return xngPortAPIUrl;
	}

	public void setXngResourceAPIUrl(String xngResourceAPIUrl) {
		this.xngPortAPIUrl = xngResourceAPIUrl;
	}

	public String getXngPortAPIUrl() {
		return xngPortAPIUrl;
	}

	public void setXngPortAPIUrl(String xngPortAPIUrl) {
		this.xngPortAPIUrl = xngPortAPIUrl;
	}

	public String getXngPortAPIUsername() {
		return xngPortAPIUsername;
	}

	public void setXngPortAPIUsername(String xngPortAPIUsername) {
		this.xngPortAPIUsername = xngPortAPIUsername;
	}

	public String getXngPortAPIPassword() {
		return xngPortAPIPassword;
	}

	public void setXngPortAPIPassword(String xngPortAPIPassword) {
		this.xngPortAPIPassword = xngPortAPIPassword;
	}

	public String getPremiseMasterLocationAPIUrl() {
		return premiseMasterLocationAPIUrl;
	}

	public void setPremiseMasterLocationAPIUrl(String premiseMasterLocationAPIUrl) {
		this.premiseMasterLocationAPIUrl = premiseMasterLocationAPIUrl;
	}

	public String getPremiseMasterLocationAPIUsername() {
		return premiseMasterLocationAPIUsername;
	}

	public void setPremiseMasterLocationAPIUsername(String premiseMasterLocationAPIUsername) {
		this.premiseMasterLocationAPIUsername = premiseMasterLocationAPIUsername;
	}

	public String getPremiseMasterLocationAPIPassword() {
		return premiseMasterLocationAPIPassword;
	}

	public void setPremiseMasterLocationAPIPassword(String premiseMasterLocationAPIPassword) {
		this.premiseMasterLocationAPIPassword = premiseMasterLocationAPIPassword;
	}

	public String getRequestAPIUrl() {
		return requestAPIUrl;
	}

	public void setRequestAPIUrl(String requestAPIUrl) {
		this.requestAPIUrl = requestAPIUrl;
	}

	public String getRequestAPIUsername() {
		return requestAPIUsername;
	}

	public void setRequestAPIUsername(String requestAPIUsername) {
		this.requestAPIUsername = requestAPIUsername;
	}

	public String getRequestAPIPassword() {
		return requestAPIPassword;
	}

	public void setRequestAPIPassword(String requestAPIPassword) {
		this.requestAPIPassword = requestAPIPassword;
	}

	public String getServiceAPIBaseUrl() {
		return serviceAPIBaseUrl;
	}

	public void setServiceAPIBaseUrl(String serviceAPIBaseUrl) {
		this.serviceAPIBaseUrl = serviceAPIBaseUrl;
	}

	public String getServiceAPIUsername() {
		return serviceAPIUsername;
	}

	public void setServiceAPIUsername(String serviceAPIUsername) {
		this.serviceAPIUsername = serviceAPIUsername;
	}

	public String getServiceAPIPassword() {
		return serviceAPIPassword;
	}

	public void setServiceAPIPassword(String serviceAPIPassword) {
		this.serviceAPIPassword = serviceAPIPassword;
	}

	public String getBillingAPIOneOffUrl() {
		return billingAPIOneOffUrl;
	}

	public void setBillingAPIOneOffUrl(String billingAPIOneOffUrl) {
		this.billingAPIOneOffUrl = billingAPIOneOffUrl;
	}

	public String getBillingAPIRecurrUrl() {
		return billingAPIRecurrUrl;
	}

	public void setBillingAPIRecurrUrl(String billingAPIRecurrUrl) {
		this.billingAPIRecurrUrl = billingAPIRecurrUrl;
	}

	public String getBillingAPIUsername() {
		return billingAPIUsername;
	}

	public void setBillingAPIUsername(String billingAPIUsername) {
		this.billingAPIUsername = billingAPIUsername;
	}

	public String getBillingAPIPassword() {
		return billingAPIPassword;
	}

	public void setBillingAPIPassword(String billingAPIPassword) {
		this.billingAPIPassword = billingAPIPassword;
	}

	public boolean isMockBlueplanet() {
		return mockBlueplanet;
	}

	public void setMockBlueplanet(boolean mockBlueplanet) {
		this.mockBlueplanet = mockBlueplanet;
	}

	public String getIntnalJBPMUrl() {
		try {
			String host = InetAddress.getLocalHost().getHostName();
			intnalJBPMUrl = intnalJBPMUrl.replaceAll("%s", host);
		} catch (UnknownHostException e) {

		}
		return intnalJBPMUrl;
	}

	public void setIntnalJBPMUrl(String intnalJBPMUrl) {
		this.intnalJBPMUrl = intnalJBPMUrl;
	}

	public String getIntnalJBPMUsername() {
		return intnalJBPMUsername;
	}

	public void setIntnalJBPMUsername(String intnalJBPMUsername) {
		this.intnalJBPMUsername = intnalJBPMUsername;
	}

	public String getIntnalJBPMPassword() {
		return intnalJBPMPassword;
	}

	public void setIntnalJBPMPassword(String intnalJBPMPassword) {
		this.intnalJBPMPassword = intnalJBPMPassword;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getSiebelUrl() {
		return siebelUrl;
	}

	public void setSiebelUrl(String siebelUrl) {
		this.siebelUrl = siebelUrl;
	}

	public String getSiebelUsername() {
		return siebelUsername;
	}

	public void setSiebelUsername(String siebelUsername) {
		this.siebelUsername = siebelUsername;
	}

	public String getSiebelPassword() {
		return siebelPassword;
	}

	public void setSiebelPassword(String siebelPassword) {
		this.siebelPassword = siebelPassword;
	}

	public String getPortDeploymentId() {
		return portDeploymentId;
	}

	public void setPortDeploymentId(String portDeploymentId) {
		this.portDeploymentId = portDeploymentId;
	}

	public String getConnectionDeploymentId() {
		return connectionDeploymentId;
	}

	public void setConnectionDeploymentId(String connectionDeploymentId) {
		this.connectionDeploymentId = connectionDeploymentId;
	}

	public String getKenanInterfaceUrl() {
		return kenanInterfaceUrl;
	}

	public void setKenanInterfaceUrl(String kenanInterfaceUrl) {
		this.kenanInterfaceUrl = kenanInterfaceUrl;
	}

	public String getKenanInterfaceUsername() {
		return kenanInterfaceUsername;
	}

	public void setKenanInterfaceUsername(String kenanInterfaceUsername) {
		this.kenanInterfaceUsername = kenanInterfaceUsername;
	}

	public String getKenanInterfacePassword() {
		return kenanInterfacePassword;
	}

	public void setKenanInterfacePassword(String kenanInterfacePassword) {
		this.kenanInterfacePassword = kenanInterfacePassword;
	}

	public String getSmartSystemUrl() {
		return smartSystemUrl;
	}

	public void setSmartSystemUrl(String smartSystemUrl) {
		this.smartSystemUrl = smartSystemUrl;
	}

	public String getSmartSystemUsername() {
		return smartSystemUsername;
	}

	public void setSmartSystemUsername(String smartSystemUsername) {
		this.smartSystemUsername = smartSystemUsername;
	}

	public String getSmartSystemPassword() {
		return smartSystemPassword;
	}

	public void setSmartSystemPassword(String smartSystemPassword) {
		this.smartSystemPassword = smartSystemPassword;
	}

	public String getInboundRestApiUrl() {
		return inboundRestApiUrl;
	}

	public void setInboundRestApiUrl(String inboundRestApiUrl) {
		this.inboundRestApiUrl = inboundRestApiUrl;
	}

	public String getInboundRestApiUserName() {
		return inboundRestApiUserName;
	}

	public void setInboundRestApiUserName(String inboundRestApiUserName) {
		this.inboundRestApiUserName = inboundRestApiUserName;
	}

	public String getInboundRestApiPassword() {
		return inboundRestApiPassword;
	}

	public void setInboundRestApiPassword(String inboundRestApiPassword) {
		this.inboundRestApiPassword = inboundRestApiPassword;
	}

	public String getAdminApiLoginUrl() {
		return adminApiLoginUrl;
	}

	public void setAdminApiLoginUrl(String adminApiLoginUrl) {
		this.adminApiLoginUrl = adminApiLoginUrl;
	}

	public String getAdminApiCustomerUrl() {
		return adminApiCustomerUrl;
	}

	public void setAdminApiCustomerUrl(String adminApiCustomerUrl) {
		this.adminApiCustomerUrl = adminApiCustomerUrl;
	}

	public String getAdminApiUserName() {
		return adminApiUserName;
	}

	public void setAdminApiUserName(String adminApiUserName) {
		this.adminApiUserName = adminApiUserName;
	}

	public String getAdminApiPassword() {
		return adminApiPassword;
	}

	public void setAdminApiPassword(String adminApiPassword) {
		this.adminApiPassword = adminApiPassword;
	}

	public String getOnboardingDeploymentId() {
		return onboardingDeploymentId;
	}

	public void setOnboardingDeploymentId(String onboardingDeploymentId) {
		this.onboardingDeploymentId = onboardingDeploymentId;
	}

	public int getMaxIterations() {
		return maxIterations;
	}

	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	public String getAzureApiUrl() {
		return azureApiUrl;
	}

	public void setAzureApiUrl(String azureApiUrl) {
		this.azureApiUrl = azureApiUrl;
	}

	public String getAzureJksPath() {
		return azureJksPath;
	}

	public void setAzureJksPath(String azureJksPath) {
		this.azureJksPath = azureJksPath;
	}

	public String getAzureJksPassword() {
		return azureJksPassword;
	}

	public void setAzureJksPassword(String azureJksPassword) {
		this.azureJksPassword = azureJksPassword;
	}

	public String getAzureSubscriptionId() {
		return azureSubscriptionId;
	}

	public void setAzureSubscriptionId(String azureSubscriptionId) {
		this.azureSubscriptionId = azureSubscriptionId;
	}

	public String getAzureApiVersionId() {
		return azureApiVersionId;
	}

	public void setAzureApiVersionId(String azureApiVersionId) {
		this.azureApiVersionId = azureApiVersionId;
	}

	public String getDcaPortDeploymentId() {
		return dcaPortDeploymentId;
	}

	public void setDcaPortDeploymentId(String dcaPortDeploymentId) {
		this.dcaPortDeploymentId = dcaPortDeploymentId;
	}

	public String getDcaConnectionDeploymentId() {
		return dcaConnectionDeploymentId;
	}

	public void setDcaConnectionDeploymentId(String dcaConnectionDeploymentId) {
		this.dcaConnectionDeploymentId = dcaConnectionDeploymentId;
	}

	public int getAzureRequestTimeOut() {
		return azureRequestTimeOut;
	}

	public void setAzureRequestTimeOut(int azureRequestTimeOut) {
		this.azureRequestTimeOut = azureRequestTimeOut;
	}

	public String getPortOrConnId() {
		return portOrConnId;
	}

	public void setPortOrConnId(String portOrConnId) {
		this.portOrConnId = portOrConnId;
	}

	public String getInternalCustomerDeploymentId() {
		return internalCustomerDeploymentId;
	}

	public void setInternalCustomerDeploymentId(String internalCustomerDeploymentId) {
		this.internalCustomerDeploymentId = internalCustomerDeploymentId;
	}

	public String getSitePortDeploymentId() {
		return sitePortDeploymentId;
	}

	public void setSitePortDeploymentId(String sitePortDeploymentId) {
		this.sitePortDeploymentId = sitePortDeploymentId;
	}

	public String getEmailFromAddress() {
		return emailFromAddress;
	}

	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}

	public String getEmailToAddress() {
		return emailToAddress;
	}

	public void setEmailToAddress(String emailToAddress) {
		this.emailToAddress = emailToAddress;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getBccEmail() {
		return bccEmail;
	}

	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}

	public String getSitePortAdminUiLink() {
		return sitePortAdminUiLink;
	}

	public void setSitePortAdminUiLink(String sitePortAdminUiLink) {
		this.sitePortAdminUiLink = sitePortAdminUiLink;
	}

	public String getConfigServiceUrl() {
		return configServiceUrl;
	}

	public void setConfigServiceUrl(String configServiceUrl) {
		this.configServiceUrl = configServiceUrl;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public boolean isXngMocked() {
		return isXngMocked;
	}

	public void setXngMocked(boolean isXngMocked) {
		this.isXngMocked = isXngMocked;
	}

	public String getAwsdCreatePortDepId() {
		return awsdCreatePortDepId;
	}

	public void setAwsdCreatePortDepId(String awsdCreatePortDepId) {
		this.awsdCreatePortDepId = awsdCreatePortDepId;
	}

	public String getAwsdDeletePortDepId() {
		return awsdDeletePortDepId;
	}

	public void setAwsdDeletePortDepId(String awsdDeletePortDepId) {
		this.awsdDeletePortDepId = awsdDeletePortDepId;
	}

	public String getAwsdCreateConnDepId() {
		return awsdCreateConnDepId;
	}

	public void setAwsdCreateConnDepId(String awsdCreateConnDepId) {
		this.awsdCreateConnDepId = awsdCreateConnDepId;
	}

	public String getAwsdModifyConnDepId() {
		return awsdModifyConnDepId;
	}

	public void setAwsdModifyConnDepId(String awsdModifyConnDepId) {
		this.awsdModifyConnDepId = awsdModifyConnDepId;
	}

	public String getAwsdDeleteConnDepId() {
		return awsdDeleteConnDepId;
	}

	public void setAwsdDeleteConnDepId(String awsdDeleteConnDepId) {
		this.awsdDeleteConnDepId = awsdDeleteConnDepId;
		System.err.println();
	}

	public String getPoAsyncUrl() {
		return poAsyncUrl;
	}

	public void setPoAsyncUrl(String poAsyncUrl) {
		try {
			String host = InetAddress.getLocalHost().getHostName();
			poAsyncUrl = poAsyncUrl.replaceAll("%s", host);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.poAsyncUrl = poAsyncUrl;
	}

	public String getPoVersion() {
		return poVersion;
	}

	public void setPoVersion(String poVersion) {
		this.poVersion = poVersion;
	}

	public String getScheduleApiUrl() {
		return scheduleApiUrl;
	}

	public void setScheduleApiUrl(String scheduleApiUrl) {
		this.scheduleApiUrl = scheduleApiUrl;
	}

	public String getScheduleApiUserName() {
		return scheduleApiUserName;
	}

	public void setScheduleApiUserName(String scheduleApiUserName) {
		this.scheduleApiUserName = scheduleApiUserName;
	}

	public String getScheduleApiPassword() {
		return scheduleApiPassword;
	}

	public void setScheduleApiPassword(String scheduleApiPassword) {
		this.scheduleApiPassword = scheduleApiPassword;
	}

	public String getScheduleApiVersion() {
		return scheduleApiVersion;
	}

	public void setScheduleApiVersion(String scheduleApiVersion) {
		this.scheduleApiVersion = scheduleApiVersion;
	}

	public String getStdDeleteConnDepId() {
		return stdDeleteConnDepId;
	}

	public void setStdDeleteConnDepId(String stdDeleteConnDepId) {
		this.stdDeleteConnDepId = stdDeleteConnDepId;
	}

	public int getAwsConnectionTimeOut() {
		return awsConnectionTimeOut;
	}

	public void setAwsConnectionTimeOut(int awsConnectionTimeOut) {
		this.awsConnectionTimeOut = awsConnectionTimeOut;
	}

	public String getStdCreateConnDepId() {
		return stdCreateConnDepId;
	}

	public void setStdCreateConnDepId(String stdCreateConnDepId) {
		this.stdCreateConnDepId = stdCreateConnDepId;
	}

	public String getAzureCreateConnDepId() {
		return azureCreateConnDepId;
	}

	public void setAzureCreateConnDepId(String azureCreateConnDepId) {
		this.azureCreateConnDepId = azureCreateConnDepId;
	}

	public String getStdModifyConnDepId() {
		return stdModifyConnDepId;
	}

	public void setStdModifyConnDepId(String stdModifyConnDepId) {
		this.stdModifyConnDepId = stdModifyConnDepId;
	}

	public String getAzureModifyConnDepId() {
		return azureModifyConnDepId;
	}

	public void setAzureModifyConnDepId(String azureModifyConnDepId) {
		this.azureModifyConnDepId = azureModifyConnDepId;
	}

	public String getAddressBookPath() {
		return addressBookPath;
	}

	public void setAddressBookPath(String addressBookPath) {
		this.addressBookPath = addressBookPath;
	}

	public String getTempLocationPath() {
		return tempLocationPath;
	}

	public void setTempLocationPath(String tempLocationPath) {
		this.tempLocationPath = tempLocationPath;
	}

	public String getCustomerCeaseOrRestoreDeploymentId() {
		return customerCeaseOrRestoreDeploymentId;
	}

	public void setCustomerCeaseOrRestoreDeploymentId(String customerCeaseOrRestoreDeploymentId) {
		this.customerCeaseOrRestoreDeploymentId = customerCeaseOrRestoreDeploymentId;
	}

	public String getInternalDeploymentId() {
		return internalDeploymentId;
	}

	public void setInternalDeploymentId(String internalDeploymentId) {
		this.internalDeploymentId = internalDeploymentId;
	}

	public String getVerizonCircuitId() {
		return verizonCircuitId;
	}

	public void setVerizonCircuitId(String verizonCircuitId) {
		this.verizonCircuitId = verizonCircuitId;
	}

	public String getVerizonBillingId() {
		return verizonBillingId;
	}

	public void setVerizonBillingId(String verizonBillingId) {
		this.verizonBillingId = verizonBillingId;
	}

	public String getCustomerAPIUserName() {
		return customerAPIUserName;
	}

	public void setCustomerAPIUserName(String customerAPIUserName) {
		this.customerAPIUserName = customerAPIUserName;
	}

	public String getCustomerAPIPassword() {
		return customerAPIPassword;
	}

	public void setCustomerAPIPassword(String customerAPIPassword) {
		this.customerAPIPassword = customerAPIPassword;
	}

	public String getCustomerAPIUrl() {
		return customerAPIUrl;
	}

	public void setCustomerAPIUrl(String customerAPIUrl) {
		this.customerAPIUrl = customerAPIUrl;
	}

	public String getOnboardingEmailToAddress() {
		return onboardingEmailToAddress;
	}

	public void setOnboardingEmailToAddress(String onboardingEmailToAddress) {
		this.onboardingEmailToAddress = onboardingEmailToAddress;
	}

	public String getAsiaOnboardingDepId() {
		return asiaOnboardingDepId;
	}

	public void setAsiaOnboardingDepId(String asiaOnboardingDepId) {
		this.asiaOnboardingDepId = asiaOnboardingDepId;
	}

	public String getAwsAsiaAccessKey() {
		return awsAsiaAccessKey;
	}

	public void setAwsAsiaAccessKey(String awsAsiaAccessKey) {
		this.awsAsiaAccessKey = awsAsiaAccessKey;
	}

	public String getAwsAsiaSecretKey() {
		return awsAsiaSecretKey;
	}

	public void setAwsAsiaSecretKey(String awsAsiaSecretKey) {
		this.awsAsiaSecretKey = awsAsiaSecretKey;
	}

	public String getMdsoAzureServiceProfile() {
		return mdsoAzureServiceProfile;
	}

	public void setMdsoAzureServiceProfile(String mdsoAzureServiceProfile) {
		this.mdsoAzureServiceProfile = mdsoAzureServiceProfile;
	}

	public String getMdsoDefaultServiceProfile() {
		return mdsoDefaultServiceProfile;
	}

	public void setMdsoDefaultServiceProfile(String mdsoDefaultServiceProfile) {
		this.mdsoDefaultServiceProfile = mdsoDefaultServiceProfile;
	}

	public String getPoAzureServiceProfile() {
		return poAzureServiceProfile;
	}

	public void setPoAzureServiceProfile(String poAzureServiceProfile) {
		this.poAzureServiceProfile = poAzureServiceProfile;
	}

	public String getPoDefaultServiceProfile() {
		return poDefaultServiceProfile;
	}

	public void setPoDefaultServiceProfile(String poDefaultServiceProfile) {
		this.poDefaultServiceProfile = poDefaultServiceProfile;
	}

	public String getAsiaToEmail() {
		return asiaToEmail;
	}

	public void setAsiaToEmail(String asiaToEmail) {
		this.asiaToEmail = asiaToEmail;
	}

	public String getMefUrl() {
		return mefUrl;
	}

	public void setMefUrl(String mefUrl) {
		this.mefUrl = mefUrl;
	}

	public String getNcBaseUrl() {
		return ncBaseUrl;
	}

	public void setNcBaseUrl(String ncBaseUrl) {
		this.ncBaseUrl = ncBaseUrl;
	}

	public String getNcUserName() {
		return ncUserName;
	}

	public void setNcUserName(String ncUserName) {
		this.ncUserName = ncUserName;
	}

	public String getNcPassword() {
		return ncPassword;
	}

	public void setNcPassword(String ncPassword) {
		this.ncPassword = ncPassword;
	}

	public String getNcStdModifyConnDepId() {
		return ncStdModifyConnDepId;
	}

	public void setNcStdModifyConnDepId(String ncStdModifyConnDepId) {
		this.ncStdModifyConnDepId = ncStdModifyConnDepId;
	}

	public String getNcAzureModifyConnDepId() {
		return ncAzureModifyConnDepId;
	}

	public void setNcAzureModifyConnDepId(String ncAzureModifyConnDepId) {
		this.ncAzureModifyConnDepId = ncAzureModifyConnDepId;
	}

	public String getNcStdDeleteConnDepId() {
		return ncStdDeleteConnDepId;
	}

	public void setNcStdDeleteConnDepId(String ncStdDeleteConnDepId) {
		this.ncStdDeleteConnDepId = ncStdDeleteConnDepId;
	}

	public String getNcAzureDeleteConnDepId() {
		return ncAzureDeleteConnDepId;
	}

	public void setNcAzureDeleteConnDepId(String ncAzureDeleteConnDepId) {
		this.ncAzureDeleteConnDepId = ncAzureDeleteConnDepId;
	}

	public String getNcAwshModifyConnDepId() {
		return ncAwshModifyConnDepId;
	}

	public void setNcAwshModifyConnDepId(String ncAwshModifyConnDepId) {
		this.ncAwshModifyConnDepId = ncAwshModifyConnDepId;
	}

	public String getNcAwshDeleteConnDepId() {
		return ncAwshDeleteConnDepId;
	}

	public void setNcAwshDeleteConnDepId(String ncAwshDeleteConnDepId) {
		this.ncAwshDeleteConnDepId = ncAwshDeleteConnDepId;
	}

	public String getNcAwsdModifyConnDepId() {
		return ncAwsdModifyConnDepId;
	}

	public void setNcAwsdModifyConnDepId(String ncAwsdModifyConnDepId) {
		this.ncAwsdModifyConnDepId = ncAwsdModifyConnDepId;
	}

	public String getNcAwsdDeleteConnDepId() {
		return ncAwsdDeleteConnDepId;
	}

	public void setNcAwsdDeleteConnDepId(String ncAwsdDeleteConnDepId) {
		this.ncAwsdDeleteConnDepId = ncAwsdDeleteConnDepId;
	}

	public String getNcInternalCustomerDeploymentId() {
		return ncInternalCustomerDeploymentId;
	}

	public void setNcInternalCustomerDeploymentId(String ncInternalCustomerDeploymentId) {
		this.ncInternalCustomerDeploymentId = ncInternalCustomerDeploymentId;
	}

	public String getLogUniqueId() {
		return logUniqueId;
	}

	public void setLogUniqueId(String logUniqueId) {
		this.logUniqueId = logUniqueId;
	}

	public String getXcCreateDepId() {
		return xcCreateDepId;
	}

	public void setXcCreateDepId(String xcCreateDepId) {
		this.xcCreateDepId = xcCreateDepId;
	}

	public String getXcDeleteDepId() {
		return xcDeleteDepId;
	}

	public void setXcDeleteDepId(String xcDeleteDepId) {
		this.xcDeleteDepId = xcDeleteDepId;
	}

	public String getXcDelay() {
		return xcDelay;
	}

	public void setXcDelay(String xcDelay) {
		this.xcDelay = xcDelay;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getSystemStatusUrl() {
		return systemStatusUrl;
	}

	public void setSystemStatusUrl(String systemStatusUrl) {
		this.systemStatusUrl = systemStatusUrl;
	}

	public String getSystemStatusUserName() {
		return systemStatusUserName;
	}

	public void setSystemStatusUserName(String systemStatusUserName) {
		this.systemStatusUserName = systemStatusUserName;
	}

	public String getSystemStatusPassword() {
		return systemStatusPassword;
	}

	public void setSystemStatusPassword(String systemStatusPassword) {
		this.systemStatusPassword = systemStatusPassword;
	}

	public String getModuleStatusPollerTime() {
		return moduleStatusPollerTime;
	}

	public void setModuleStatusPollerTime(String moduleStatusPollerTime) {
		this.moduleStatusPollerTime = moduleStatusPollerTime;
	}

	public String getStdDeletePortDepId() {
		return stdDeletePortDepId;
	}

	public void setStdDeletePortDepId(String stdDeletePortDepId) {
		this.stdDeletePortDepId = stdDeletePortDepId;
	}

	public String getAzureDeletePortDepId() {
		return azureDeletePortDepId;
	}

	public void setAzureDeletePortDepId(String azureDeletePortDepId) {
		this.azureDeletePortDepId = azureDeletePortDepId;
	}

	public String getDeletePollerDelay() {
		return deletePollerDelay;
	}

	public void setDeletePollerDelay(String deletePollerDelay) {
		this.deletePollerDelay = deletePollerDelay;
	}

	public String getMefStdCreateConnDepId() {
		return mefStdCreateConnDepId;
	}

	public void setMefStdCreateConnDepId(String mefStdCreateConnDepId) {
		this.mefStdCreateConnDepId = mefStdCreateConnDepId;
	}

	public String getMefAwshCreatConnDeId() {
		return mefAwshCreatConnDeId;
	}

	public void setMefAwshCreatConnDeId(String mefAwshCreatConnDeId) {
		this.mefAwshCreatConnDeId = mefAwshCreatConnDeId;
	}

	public String getLocationApiUrl() {
		return PropertyReader.getLocationAPIBaseUrl(this);
	}

	public void setLocationApiUrl(String locationApiUrl) {
		this.locationApiUrl = locationApiUrl;
	}

	public String getIbmBaseUrl() {
		return ibmBaseUrl;
	}

	public void setIbmBaseUrl(String ibmBaseUrl) {
		this.ibmBaseUrl = ibmBaseUrl;
	}

	public String getIbmCreatePortDepId() {
		return ibmCreatePortDepId;
	}

	public void setIbmCreatePortDepId(String ibmCreatePortDepId) {
		this.ibmCreatePortDepId = ibmCreatePortDepId;
	}

	public String getIbmDeletePortDepId() {
		return ibmDeletePortDepId;
	}

	public void setIbmDeletePortDepId(String ibmDeletePortDepId) {
		this.ibmDeletePortDepId = ibmDeletePortDepId;
	}

	public String getIbmCreateConnDepId() {
		return ibmCreateConnDepId;
	}

	public void setIbmCreateConnDepId(String ibmCreateConnDepId) {
		this.ibmCreateConnDepId = ibmCreateConnDepId;
	}

	public String getIbmModifyConnDepId() {
		return ibmModifyConnDepId;
	}

	public void setIbmModifyConnDepId(String ibmModifyConnDepId) {
		this.ibmModifyConnDepId = ibmModifyConnDepId;
	}

	public String getIbmDeleteConnDepId() {
		return ibmDeleteConnDepId;
	}

	public void setIbmDeleteConnDepId(String ibmDeleteConnDepId) {
		this.ibmDeleteConnDepId = ibmDeleteConnDepId;
	}

	public String getRequestDependencyBaseUrl() {
		return requestDependencyBaseUrl;
	}

	public void setRequestDependencyBaseUrl(String requestDependencyBaseUrl) {
		this.requestDependencyBaseUrl = requestDependencyBaseUrl;
	}

	public String getOracleBaseUrl() {
		return oracleBaseUrl;
	}

	public void setOracleBaseUrl(String oracleBaseUrl) {
		this.oracleBaseUrl = oracleBaseUrl;
	}

	public String getOracleCreatePortDepId() {
		return oracleCreatePortDepId;
	}

	public void setOracleCreatePortDepId(String oracleCreatePortDepId) {
		this.oracleCreatePortDepId = oracleCreatePortDepId;
	}

	public String getOracleDeletePortDepId() {
		return oracleDeletePortDepId;
	}

	public void setOracleDeletePortDepId(String oracleDeletePortDepId) {
		this.oracleDeletePortDepId = oracleDeletePortDepId;
	}

	public String getOracleCreateConnDepId() {
		return oracleCreateConnDepId;
	}

	public void setOracleCreateConnDepId(String oracleCreateConnDepId) {
		this.oracleCreateConnDepId = oracleCreateConnDepId;
	}

	public String getOracleModifyConnDepId() {
		return oracleModifyConnDepId;
	}

	public void setOracleModifyConnDepId(String oracleModifyConnDepId) {
		this.oracleModifyConnDepId = oracleModifyConnDepId;
	}

	public String getOracleDeleteConnDepId() {
		return oracleDeleteConnDepId;
	}

	public void setOracleDeleteConnDepId(String oracleDeleteConnDepId) {
		this.oracleDeleteConnDepId = oracleDeleteConnDepId;
	}

	public String getTicketNotificationEmailTo() {
		return ticketNotificationEmailTo;
	}

	public void setTicketNotificationEmailTo(String ticketNotificationEmailTo) {
		this.ticketNotificationEmailTo = ticketNotificationEmailTo;
	}
	
	public String getPlannedWorksCCEmail() {
		return plannedWorksCCEmail;
	}
	
	public void setPlannedWorksCCEmail(String plannedWorksCCEmail) {
		this.plannedWorksCCEmail = plannedWorksCCEmail;
	}
	
	public String getPlannedWorksToEmail() {
		return plannedWorksToEmail;
	}
	
	public void setPlannedWorksToEmail(String plannedWorksToEmail) {
		this.plannedWorksToEmail = plannedWorksToEmail;
	}

	public String getEquinixBaseUrl() {
		return equinixBaseUrl;
	}

	public void setEquinixBaseUrl(String equinixBaseUrl) {
		this.equinixBaseUrl = equinixBaseUrl;
	}

	public String getEquinixCreatePortDepId() {
		return equinixCreatePortDepId;
	}

	public void setEquinixCreatePortDepId(String equinixCreatePortDepId) {
		this.equinixCreatePortDepId = equinixCreatePortDepId;
	}

	public String getEquinixDeletePortDepId() {
		return equinixDeletePortDepId;
	}

	public void setEquinixDeletePortDepId(String equinixDeletePortDepId) {
		this.equinixDeletePortDepId = equinixDeletePortDepId;
	}

	public String getEquinixCreateConnDepId() {
		return equinixCreateConnDepId;
	}

	public void setEquinixCreateConnDepId(String equinixCreateConnDepId) {
		this.equinixCreateConnDepId = equinixCreateConnDepId;
	}

	public String getEquinixModifyConnDepId() {
		return equinixModifyConnDepId;
	}

	public void setEquinixModifyConnDepId(String equinixModifyConnDepId) {
		this.equinixModifyConnDepId = equinixModifyConnDepId;
	}

	public String getEquinixDeleteConnDepId() {
		return equinixDeleteConnDepId;
	}

	public void setEquinixDeleteConnDepId(String equinixDeleteConnDepId) {
		this.equinixDeleteConnDepId = equinixDeleteConnDepId;
	}

}
