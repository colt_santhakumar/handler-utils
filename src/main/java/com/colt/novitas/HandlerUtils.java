package com.colt.novitas;


import org.kie.api.runtime.process.WorkItem;

public class HandlerUtils {
	
	public static boolean isTest(WorkItem workItem) {
		return getEnvironmentProperties(workItem).isMockBlueplanet();
		
	}
	
	public static WorkflowType getWorkflowType(WorkItem workItem) {
		String workflow = (String) workItem.getParameter("workflow_type");
		if ("create_port".equals(workflow)) {
			return WorkflowType.CREATE_PORT;
		}
		else if ("delete_port".equals(workflow)) {
			return WorkflowType.DELETE_PORT;
		}
		else if ("create_connection".equals(workflow)) {
			return WorkflowType.CREATE_CONNECTION;
		}
		else if ("modify_connection".equals(workflow)) {
			return WorkflowType.MODIFY_CONNECTION;
		}
		else if ("delete_connection".equals(workflow)) {
			return WorkflowType.DELETE_CONNECTION;
		}
		else if ("onboarding".equals(workflow)) {
			return WorkflowType.ONBOARDING;
		}
		return WorkflowType.UNDEFINED;
		
	}
	
	public static EnvironmentProperties getEnvironmentProperties(WorkItem workItem) {
		String propertyFile = (String) workItem.getParameter("property_file");
		return new PropertyReader(propertyFile).getDefaultProperties();
		
	}
	
	public static Integer getIntValue(Object value) {
		
		if (value == null) {
			return null;
		}
		
		if (value instanceof Integer) {
			return (Integer) value;
		}
		
		return Integer.valueOf(String.valueOf(value));
	}
	
	public static Exception throwHandlerExcetpion(Exception e) {
		//System.err.println(e);
		//e.printStackTrace();
		if (e instanceof RuntimeException) {
			throw (RuntimeException)e;
		}
		throw new RuntimeException(e.getMessage(), e);
	}
	
	public static void main(String[] args) {
		
		
	}
	
}
