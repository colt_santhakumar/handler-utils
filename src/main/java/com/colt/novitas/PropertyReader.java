package com.colt.novitas;

import com.colt.novitas.resource.client.ResourceAPIClient;
import com.colt.novitas.resource.response.Property;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyReader
{
  private static final String FILE_PATH_PROPERTY = "JBPM_ENV";
  private static final String DEFAULT_PROPERTY_FILE = "/jbpm-env.properties";
  private static final String RESOURCE_API_URL_KEY = "resource.new.api.url";
  private static final String RESOURCE_API_ENV_KEY = "resource.new.api.env";
  private String filePath;
  private boolean loadFromFile;
  private Map<String, String> propertiesMap = new HashMap();

  public PropertyReader(String filePath)
  {
    this.filePath = filePath;
  }

  public PropertyReader() {}
  
  public static String getLocationAPIBaseUrl(EnvironmentProperties envProp) {
      ResourceAPIClient client = new ResourceAPIClient( envProp.getConfigServiceUrl());
      Property property = client.getProperty(envProp.getEnvironment(), PropertyKeys.LOCATION_API_BASE_URL.value());
      return null != property ? property.getValue() : null;
  }

  public EnvironmentProperties getDefaultProperties()
  {
    Properties properties = loadProperties();
    EnvironmentProperties environmentProp = new EnvironmentProperties();
    if (!this.loadFromFile)
    {
      String configServiceUrl = properties.getProperty("resource.new.api.url");
      String environment = properties.getProperty("resource.new.api.env");
      environmentProp.setConfigServiceUrl(configServiceUrl);
      environmentProp.setEnvironment(environment);
      ResourceAPIClient client = new ResourceAPIClient(configServiceUrl);
      this.propertiesMap = client.getPropertiesAsMap(environment, PropertyKeys.getKeys());
    }

    environmentProp.setSmtpHost(getProperty(PropertyKeys.SMTP_HOST.value(), properties));
    environmentProp.setSmtpPort(getProperty(PropertyKeys.SMTP_PORT.value(), properties));
    environmentProp.setSmtpUserName(getProperty(PropertyKeys.SMTP_USER.value(), properties));
    environmentProp.setSmtpPassword(getProperty(PropertyKeys.SMTP_PASSWORD.value(), properties));

    String starTls = getProperty(PropertyKeys.SMTP_STARTLS.value());
    if ((starTls != null)) {
      environmentProp.setSmtpStarTls(Boolean.valueOf(starTls));
    }

    environmentProp.setRequestAPIUrl(getProperty(PropertyKeys.REQUEST_API_URL.value(), properties));
    environmentProp.setRequestAPIUsername(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(), properties));
    environmentProp.setRequestAPIPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(), properties));

    environmentProp.setServiceAPIBaseUrl(getProperty(PropertyKeys.SERVICE_API_BASE_URL.value(), properties));
    environmentProp.setServiceAPIUsername(getProperty(PropertyKeys.SERVICE_API_USERNAME.value(), properties));
    environmentProp.setServiceAPIPassword(getProperty(PropertyKeys.SERVICE_API_PASS.value(), properties));

    environmentProp.setBillingAPIOneOffUrl(getProperty(PropertyKeys.BILLING_API_ONEOFF_URL.value(), properties));
    environmentProp.setBillingAPIRecurrUrl(getProperty(PropertyKeys.BILLING_API_RECURR_URL.value(), properties));
    environmentProp.setBillingAPIUsername(getProperty(PropertyKeys.BILLING_API_USERNAME.value(), properties));
    environmentProp.setBillingAPIPassword(getProperty(PropertyKeys.BILLING_API_PASS.value(), properties));

    environmentProp.setInboundRestApiUrl(getProperty(PropertyKeys.INBOUND_API_URL.value(), properties));
    environmentProp.setInboundRestApiUserName(getProperty(PropertyKeys.INBOUND_API_USERNAME.value(), properties));
    environmentProp.setInboundRestApiPassword(getProperty(PropertyKeys.INBOUND_API_PASS.value(), properties));

    environmentProp.setSiebelUrl(getProperty(PropertyKeys.SIEBEL_URL.value(), properties));
    environmentProp.setSiebelUsername(getProperty(PropertyKeys.SIEBEL_USERNAME.value(), properties));
    environmentProp.setSiebelPassword(getProperty(PropertyKeys.SIEBEL_PASSWORD.value(), properties));

    environmentProp.setSmartSystemUrl(getProperty(PropertyKeys.SMART_SYSTEM_URL.value(), properties));
    environmentProp.setSmartSystemUsername(getProperty(PropertyKeys.SMART_SYSTEM_USERNAME.value(), properties));
    environmentProp.setSmartSystemPassword(getProperty(PropertyKeys.SMART_SYSTEM_PASS.value(), properties));

    environmentProp.setKenanInterfaceUrl(getProperty(PropertyKeys.KENAN_INF_URL.value(), properties));
    environmentProp.setKenanInterfaceUsername(getProperty(PropertyKeys.KENAN_INF_USERNAME.value(), properties));
    environmentProp.setKenanInterfacePassword(getProperty(PropertyKeys.KENAN_INF_PASS.value(), properties));

    environmentProp.setCallBackUrl(getProperty(PropertyKeys.CALLBACK_URL.value(), properties));

    environmentProp.setXngPortAPIUrl(getProperty(PropertyKeys.XNG_PORT_API_URL.value(), properties));
    environmentProp.setXngPortAPIUsername(getProperty(PropertyKeys.XNG_PORT_API_USERNAME.value(), properties));
    environmentProp.setXngPortAPIPassword(getProperty(PropertyKeys.XNG_PORT_API_PASS.value(), properties));

    environmentProp.setXngConnectionAPIUrl(getProperty(PropertyKeys.XNG_CONN_API_URL.value(), properties));
    environmentProp.setXngConnectionAPIUsername(getProperty(PropertyKeys.XNG_CONN_API_USERNAME.value(), properties));
    environmentProp.setXngConnectionAPIPassword(getProperty(PropertyKeys.XNG_CONN_API_PASS.value(), properties));

    environmentProp.setPremiseMasterLocationAPIUrl(getProperty(PropertyKeys.PREMISE_MST_LOC_API_URL.value(), properties));
    environmentProp.setPremiseMasterLocationAPIUsername(getProperty(PropertyKeys.PREMISE_MST_LOC_API_USERNAME.value(), properties));
    environmentProp.setPremiseMasterLocationAPIPassword(getProperty(PropertyKeys.PREMISE_MST_LOC_API_PASSWORD.value(), properties));

    environmentProp.setPortDeploymentId(getProperty(PropertyKeys.PORT_DEPLOY_ID.value(), properties));
    environmentProp.setConnectionDeploymentId(getProperty(PropertyKeys.CONN_DEPLOY_ID.value(), properties));
    environmentProp.setOnboardingDeploymentId(getProperty(PropertyKeys.ONBOAR_DEPLOY_ID.value(), properties));
    environmentProp.setDcaPortDeploymentId(getProperty(PropertyKeys.DCA_PORT_DEPLOY_ID.value(), properties));
    environmentProp.setDcaConnectionDeploymentId(getProperty(PropertyKeys.DCA_CONN_DEPLOY_ID.value(), properties));

    environmentProp.setAdminApiLoginUrl(getProperty(PropertyKeys.ADMIN_API_LOGIN_URL.value(), properties));
    environmentProp.setAdminApiCustomerUrl(getProperty(PropertyKeys.ADMIN_API_CUSTOMER_URL.value(), properties));
    environmentProp.setAdminApiUserName(getProperty(PropertyKeys.ADMIN_API_USERNAME.value(), properties));
    environmentProp.setAdminApiPassword(getProperty(PropertyKeys.ADMIN_API_PASS.value(), properties));

    environmentProp.setJbpmUrl(getProperty(PropertyKeys.JBPM_URL.value(), properties));
    environmentProp.setAwshCreatePortDepId(getProperty(PropertyKeys.AWSH_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setAwshDeletePortDepId(getProperty(PropertyKeys.AWSH_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setAwshCreateConnDepId(getProperty(PropertyKeys.AWSH_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setAwshModifyConnDepId(getProperty(PropertyKeys.AWSH_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setAwshDeleteConnDepId(getProperty(PropertyKeys.AWSH_DELETE_CONN_DEP_ID.value(), properties));
    environmentProp.setAwsdCreatePortDepId(getProperty(PropertyKeys.AWSD_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setAwsdDeletePortDepId(getProperty(PropertyKeys.AWSD_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setAwsdCreateConnDepId(getProperty(PropertyKeys.AWSD_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setAwsdModifyConnDepId(getProperty(PropertyKeys.AWSD_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setAwsdDeleteConnDepId(getProperty(PropertyKeys.AWSD_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setGcpCreatePortDepId(getProperty(PropertyKeys.GCP_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setGcpDeletePortDepId(getProperty(PropertyKeys.GCP_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setGcpCreateConnDepId(getProperty(PropertyKeys.GCP_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setGcpModifyConnDepId(getProperty(PropertyKeys.GCP_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setGcpDeleteConnDepId(getProperty(PropertyKeys.GCP_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setIbmBaseUrl(getProperty(PropertyKeys.IBM_BASE_URL.value(),properties));
    environmentProp.setIbmCreatePortDepId(getProperty(PropertyKeys.IBM_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setIbmDeletePortDepId(getProperty(PropertyKeys.IBM_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setIbmCreateConnDepId(getProperty(PropertyKeys.IBM_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setIbmModifyConnDepId(getProperty(PropertyKeys.IBM_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setIbmDeleteConnDepId(getProperty(PropertyKeys.IBM_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setAwsAccessKey(getProperty(PropertyKeys.AWS_ACCESS_KEY.value(), properties));
    environmentProp.setAwsSecretKey(getProperty(PropertyKeys.AWS_SECRET_KEY.value(), properties));
    environmentProp.setAwsAsiaAccessKey(getProperty(PropertyKeys.AWS_ASIA_ACCESS_KEY.value(), properties));
    environmentProp.setAwsAsiaSecretKey(getProperty(PropertyKeys.AWS_ASIA_SECRET_KEY.value(), properties));
    environmentProp.setPollerDelay(getProperty(PropertyKeys.AWS_STATUS_POLLER_DELAY.value(), properties));
    environmentProp.setDeletePollerDelay(getProperty(PropertyKeys.AWS_DELETE_POLLER_DELAY.value(), properties));
    String mockString = getProperty(PropertyKeys.MOCK_BLUE_PLANET.value(), properties);
    if ((mockString != null) && ("true".equalsIgnoreCase(mockString.trim()))) {
      environmentProp.setMockBlueplanet(true);
    }
    environmentProp.setIntnalJBPMUrl(getProperty(PropertyKeys.INTERNAL_JBPM_URL.value(), properties));
    environmentProp.setIntnalJBPMUsername(getProperty(PropertyKeys.INTERNAL_JBPM_USERNAME.value(), properties));
    environmentProp.setIntnalJBPMPassword(getProperty(PropertyKeys.INTERNAL_JBPM_PASS.value(), properties));

    environmentProp.setInternalDeploymentId(getProperty(PropertyKeys.INTERNAL_DEPLOYMENT_ID.value(), properties));
    environmentProp.setInternalCustomerDeploymentId(getProperty(PropertyKeys.INTERNAL_CUSTOMER_DEPLOYMENT_ID.value(), properties));
    environmentProp.setNcInternalCustomerDeploymentId(getProperty(PropertyKeys.INTERNAL_NC_CUSTOMER_DEPLOYMENT_ID.value(), properties));
    environmentProp.setCustomerCeaseOrRestoreDeploymentId(getProperty(PropertyKeys.CEASE_OR_RESTORE_DEPLOYMENT_ID.value(), properties));

    environmentProp.setAzureApiUrl(getProperty(PropertyKeys.AZURE_URL.value(), properties));
    environmentProp.setAzureJksPath(getProperty(PropertyKeys.AZURE_JKS_PATH.value(), properties));
    environmentProp.setAzureJksPassword(getProperty(PropertyKeys.AZURE_JKS_PASSWORD.value(), properties));
    environmentProp.setAzureSubscriptionId(getProperty(PropertyKeys.AZURE_SUBSCRIPTION_ID.value(), properties));
    environmentProp.setAzureApiVersionId(getProperty(PropertyKeys.AZURE_API_VERSION_ID.value(), properties));
    String azureTimeOut = getProperty(PropertyKeys.AZURE_TIME_OUT.value(), properties);
    if ((null != azureTimeOut) && (!azureTimeOut.isEmpty())) {
      environmentProp.setAzureRequestTimeOut(Integer.valueOf(azureTimeOut).intValue());
    }
    environmentProp.setPoAsyncUrl(getProperty(PropertyKeys.PO_ASYNC_URL.value(), properties));
    environmentProp.setPoVersion(getProperty(PropertyKeys.PO_VERSION.value(), properties));
    environmentProp.setSitePortDeploymentId(getProperty(PropertyKeys.SITEPORT_DEPLOYMENT_ID.value(), properties));
    environmentProp.setEmailFromAddress(getProperty(PropertyKeys.XTRAC_FROM_EMAIL.value(), properties));
    environmentProp.setEmailToAddress(getProperty(PropertyKeys.XTRAC_TO_EMAIL.value(), properties));
    environmentProp.setCcEmail(getProperty(PropertyKeys.XTRAC_CC_EMAIL.value(), properties));
    environmentProp.setBccEmail(getProperty(PropertyKeys.XTRAC_BCC_EMAIL.value(), properties));
    environmentProp.setSitePortAdminUiLink(getProperty(PropertyKeys.SITEPORT_ADMIN_UI_LINK.value(), properties));
    String isAwsMocked = getProperty(PropertyKeys.AWS_MOCKED.value(), properties);
    environmentProp.setAWSMocked(null == isAwsMocked ? true : Boolean.valueOf(isAwsMocked));
    String isXngMocked = getProperty(PropertyKeys.XNG_MOCKED.value(), properties);
    environmentProp.setXngMocked(null == isXngMocked ? true :  Boolean.valueOf(isXngMocked));
    String maxIterations = getProperty("bpo.maxiterations", properties);
    if ((null != maxIterations) && (!"".equals(maxIterations))) {
      environmentProp.setMaxIterations(Integer.valueOf(maxIterations).intValue());
    } else {
      environmentProp.setMaxIterations(99);
    }
    String timeOut = getProperty(PropertyKeys.AWS_CONNECTION_TIME_OUT.value(), properties);
    if ((null != timeOut) && (!"".equals(timeOut))) {
      environmentProp.setAwsConnectionTimeOut(Integer.valueOf(timeOut).intValue());
    } else {
      environmentProp.setAwsConnectionTimeOut(60000);
    }
    environmentProp.setScheduleApiUrl(getProperty(PropertyKeys.SCEDULE_API_URL.value(), properties));
    environmentProp.setScheduleApiUserName(getProperty(PropertyKeys.SCHEDULE_API_USERNAME.value(), properties));
    environmentProp.setScheduleApiPassword(getProperty(PropertyKeys.SCHEDULE_API_PASSWORD.value(), properties));
    environmentProp.setScheduleApiVersion(getProperty(PropertyKeys.SCHEDULE_API_VERSION.value(), properties));
    environmentProp.setStdDeleteConnDepId(getProperty(PropertyKeys.STD_DELETE_CONN_DEP_ID.value(), properties));
    environmentProp.setAzureDeleteConnDepId(getProperty(PropertyKeys.AZURE_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setStdModifyConnDepId(getProperty(PropertyKeys.STD_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setAzureModifyConnDepId(getProperty(PropertyKeys.AZURE_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setStdCreateConnDepId(getProperty(PropertyKeys.STD_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setAzureCreateConnDepId(getProperty(PropertyKeys.AZURE_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setAddressBookPath(getProperty(PropertyKeys.ADDRESS_BOOK_BASE_URL.value(), properties));
    environmentProp.setTempLocationPath(getProperty(PropertyKeys.TEMP_LOCATION_BASE_URL.value(), properties));

    String retries = getProperty(PropertyKeys.MDSO_RECOVERY_MAX_RETRIES.value(), properties);
    if ((null != retries) && (!"".equals(retries))) {
      environmentProp.setMdsoRecoveryMaxRetries(Integer.valueOf(retries));
    } else {
      environmentProp.setMdsoRecoveryMaxRetries(Integer.valueOf(5));
    }
    environmentProp.setCustomerAPIPassword(getProperty(PropertyKeys.CUSTOMER_API_PASSWORD.value(), properties));
    environmentProp.setCustomerAPIUserName(getProperty(PropertyKeys.CUSTOMER_API_USER_NAME.value(), properties));
    environmentProp.setCustomerAPIUrl(getProperty(PropertyKeys.CUSTOMER_API_URL.value(), properties));
    environmentProp.setOnboardingEmailToAddress(getProperty(PropertyKeys.ONBOARDING_TO_EMAIL.value(), properties));
    environmentProp.setVerizonCircuitId(getProperty(PropertyKeys.VERIZON_CIRCUIT_ID.value(), properties));
    environmentProp.setVerizonBillingId(getProperty(PropertyKeys.VERIZON_BILLING_ID.value(), properties));
    environmentProp.setAsiaOnboardingDepId(getProperty(PropertyKeys.ASIA_ONBOAR_DEPLOY_ID.value(), properties));

    environmentProp.setMdsoDefaultServiceProfile(getProperty(PropertyKeys.MDSO_DEFAULT_SERVICE_PROFILE.value(), properties));
    environmentProp.setMdsoAzureServiceProfile(getProperty(PropertyKeys.MDSO_AZURE_SERVICE_PROFILE.value(), properties));
    environmentProp.setPoDefaultServiceProfile(getProperty(PropertyKeys.PO_DEFAULT_SERVICE_PROFILE.value(), properties));
    environmentProp.setPoAzureServiceProfile(getProperty(PropertyKeys.PO_AZURE_SERVICE_PROFILE.value(), properties));
    environmentProp.setAsiaToEmail(getProperty(PropertyKeys.XTRAC_ASIA_TO_EMAIL.value(), properties));
    environmentProp.setMefAwshCreatConnDeId(getProperty("mef.awsh.create.conn.depid"));
    environmentProp.setMefStdCreateConnDepId(getProperty("mef.std.create.conn.depid"));
    environmentProp.setMefUrl(getProperty(PropertyKeys.MEF_API_URL.value(), properties));
    environmentProp.setNcBaseUrl(getProperty(PropertyKeys.NC_SERVICE_API_URL.value(), properties));
    environmentProp.setNcUserName(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(), properties));
    environmentProp.setNcPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(), properties));

    environmentProp.setNcStdModifyConnDepId(getProperty(PropertyKeys.NC_STD_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setNcStdDeleteConnDepId(getProperty(PropertyKeys.NC_STD_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setNcAzureModifyConnDepId(getProperty(PropertyKeys.NC_AZURE_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setNcAzureDeleteConnDepId(getProperty(PropertyKeys.NC_AZURE_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setNcAwshModifyConnDepId(getProperty(PropertyKeys.NC_AWSH_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setNcAwshDeleteConnDepId(getProperty(PropertyKeys.NC_AWSH_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setNcAwsdModifyConnDepId(getProperty(PropertyKeys.NC_AWSD_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setNcAwsdDeleteConnDepId(getProperty(PropertyKeys.NC_AWSD_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setXcCreateDepId(getProperty(PropertyKeys.CROSS_CONNECT_CREATE_DEP_ID.value(), properties));
    environmentProp.setXcDeleteDepId(getProperty(PropertyKeys.CROSS_CONNECT_DELETE_DEP_ID.value(), properties));
    environmentProp.setXcDelay(getProperty(PropertyKeys.CROSS_CONNECT_DELAY.value(), properties));

    environmentProp.setSystemStatusUrl(getProperty(PropertyKeys.SYSTEM_STATUS_API_URL.value(), properties));
    environmentProp.setSystemStatusUserName(getProperty(PropertyKeys.SYSTEM_STATUS_API_USERNAME.value(), properties));
    environmentProp.setSystemStatusPassword(getProperty(PropertyKeys.SYSTEM_STATUS_API_PASS.value(), properties));
    environmentProp.setModuleStatusPollerTime(getProperty(PropertyKeys.MODULE_STAUTS_POLLER_TIME.value(), properties));

    environmentProp.setAzureDeletePortDepId(getProperty(PropertyKeys.AZURE_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setStdDeletePortDepId(getProperty(PropertyKeys.STD_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setLocationApiUrl(getProperty(PropertyKeys.LOCATION_API_BASE_URL.value(), properties));
    environmentProp.setErrorToEmail(getProperty(PropertyKeys.ERROR_TRIAGE_EMAIL.value(),properties));
    environmentProp.setResourceNewApiUrl(getProperty(PropertyKeys.RESOURCE_NEW_API_URL.value(), properties));
    environmentProp.setGcpBaseUrl(getProperty(PropertyKeys.GCP_BASE_URL.value(),properties));

    environmentProp.setAtosToEmail(getProperty(PropertyKeys.ATOS_TO_EMAIL.value(),properties));
    environmentProp.setAtosConnDepId(getProperty(PropertyKeys.ATOS_CONN_DEP_ID.value(),properties));

    // IP Access
    environmentProp.setIpaccessAPIBaseUrl(getProperty(PropertyKeys.IPA_BASE_URL.value(),properties));
    environmentProp.setIpaccessAPIUsername(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(), properties));
    environmentProp.setIpaccessAPIPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(), properties));
    environmentProp.setIpaccessCreateConnDepId(getProperty(PropertyKeys.IPA_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setIpaccessModifyConnDepId(getProperty(PropertyKeys.IPA_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setIpaccessDeleteConnDepId(getProperty(PropertyKeys.IPA_DELETE_CONN_DEP_ID.value(), properties));

    environmentProp.setJuniperSARAPIBaseUrl(getProperty(PropertyKeys.JUNIPER_SAR_BASE_URL.value(),properties));
    environmentProp.setJuniperSARAPIUsername(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(), properties));
    environmentProp.setJuniperSARAPIPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(), properties));

    environmentProp.setEipAPIBaseUrl(getProperty(PropertyKeys.IPA_EIP_BASE_URL.value(),properties));
    environmentProp.setEipAPIUsername(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(),properties));
    environmentProp.setEipAPIPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(),properties));

    environmentProp.setXngIPACSPName(getProperty(PropertyKeys.XNG_RESERVE_VLAN_CSP_NAME.value(), properties));
    environmentProp.setXngBaseUrl(getProperty(PropertyKeys.XNG_BASE_URL.value(), properties));
    environmentProp.setXngUsername(getProperty(PropertyKeys.REQUEST_API_USERNAME.value(), properties));
    environmentProp.setXngPassword(getProperty(PropertyKeys.REQUEST_API_PASS.value(), properties));

    environmentProp.setIpaccessNNIVLANMapping(getProperty(PropertyKeys.IPA_VLAN_MAPPING.value(), properties));
    environmentProp.setIpaccessNNIVLANType(getProperty(PropertyKeys.IPA_VLAN_TYPE.value(), properties));
    environmentProp.setJuniperSARDeviceCredential(getProperty(PropertyKeys.JUNIPER_SAR_DEVICE_CREDENTIALS.value(), properties));

    // Spark
    environmentProp.setSiebelServiceBaseUrl(getProperty(PropertyKeys.SIEBEL_SERVICE_BASE_URL.value(), properties));
    environmentProp.setSiebelServicePassword(getProperty(PropertyKeys.SIEBEL_SERVICE_PASSWORD.value(), properties));
    environmentProp.setSiebelServiceUsername(getProperty(PropertyKeys.SIEBEL_SERVICE_USERNAME.value(), properties));
    environmentProp.setSparkCapacityToEmail(getProperty(PropertyKeys.SPARKS_CAP_TO_EMAIL.value(), properties));

    environmentProp.setRequestDependencyBaseUrl(getProperty(PropertyKeys.REQUEST_DEPENDENCY_BASE_URL.value(),properties));

    environmentProp.setOracleBaseUrl(getProperty(PropertyKeys.ORACLE_BASE_URL.value(),properties));
    environmentProp.setOracleCreatePortDepId(getProperty(PropertyKeys.ORACLE_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setOracleDeletePortDepId(getProperty(PropertyKeys.ORACLE_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setOracleCreateConnDepId(getProperty(PropertyKeys.ORACLE_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setOracleModifyConnDepId(getProperty(PropertyKeys.ORACLE_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setOracleDeleteConnDepId(getProperty(PropertyKeys.ORACLE_DELETE_CONN_DEP_ID.value(), properties));
    
    environmentProp.setDeviceRemovalEmailCountries(getProperty(PropertyKeys.PORT_DEVICE_REMOVAL_EMAIL_COUNTRIES.value(), properties));
    environmentProp.setDeviceRemovalEmailCc(getProperty(PropertyKeys.PORT_DEVICE_REMOVAL_EMAIL_CC.value(), properties));
    environmentProp.setDeviceRemovalEmailTo(getProperty(PropertyKeys.PORT_DEVICE_REMOVAL_EMAIL_TO.value(), properties));
    environmentProp.setDeviceRemovalEmailFrom(getProperty(PropertyKeys.PORT_DEVICE_REMOVAL_EMAIL_FROM.value(), properties));
    
    environmentProp.setTicketNotificationEmailTo(getProperty(PropertyKeys.TICKET_INTERNAL_EMAIL_TO.value(), properties));
    environmentProp.setPlannedWorksToEmail(getProperty(PropertyKeys.ONBOARDING_PLANNED_WORKS_CONTACT_NOTIFY_EMAIL_TO.value(), properties));
    environmentProp.setPlannedWorksCCEmail(getProperty(PropertyKeys.ONBOARDING_PLANNED_WORKS_CONTACT_NOTIFY_EMAIL_CC.value(), properties));

    environmentProp.setEquinixBaseUrl(getProperty(PropertyKeys.EQUINIX_BASE_URL.value(), properties) + "/v1");
    environmentProp.setEquinixCreatePortDepId(getProperty(PropertyKeys.EQUINIX_CREATE_PORT_DEP_ID.value(), properties));
    environmentProp.setEquinixDeletePortDepId(getProperty(PropertyKeys.EQUINIX_DELETE_PORT_DEP_ID.value(), properties));
    environmentProp.setEquinixCreateConnDepId(getProperty(PropertyKeys.EQUINIX_CREATE_CONN_DEP_ID.value(), properties));
    environmentProp.setEquinixModifyConnDepId(getProperty(PropertyKeys.EQUINIX_MODIFY_CONN_DEP_ID.value(), properties));
    environmentProp.setEquinixDeleteConnDepId(getProperty(PropertyKeys.EQUINIX_DELETE_CONN_DEP_ID.value(), properties));

    return environmentProp;
  }

  public String getProperty(String property)
  {
    return getProperty(property, null);
  }

  public String getProperty(String property, Properties prop)
  {
    String val = null;
    if (this.loadFromFile)
    {
      if (prop == null) {
        prop = loadProperties();
      }
      val = prop.getProperty(property);
    }
    else
    {
      val = (String)this.propertiesMap.get(property);
    }
    return val;
  }

  public Properties loadProperties()
  {
    Properties prop = new Properties();
    InputStream input = null;
    try
    {
      String currentUsersHomeDir;
      if ((this.filePath == null) || ("".equals(this.filePath.trim()))) {
        if ((System.getProperty("JBPM_ENV") != null) &&
          (!"".equals(System.getProperty("JBPM_ENV"))))
        {
          this.filePath = System.getProperty("JBPM_ENV");
        }
        else if ((System.getenv("JBPM_ENV") != null) &&
          (!"".equals(System.getenv("JBPM_ENV"))))
        {
          this.filePath = System.getenv("JBPM_ENV");
        }
        else
        {
          currentUsersHomeDir = System.getProperty("user.home");
          this.filePath = (currentUsersHomeDir + "/jbpm-env.properties");
        }
      }
      input = new FileInputStream(this.filePath);

      prop.load(input);

      return prop;
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      if (input != null) {
        try
        {
          input.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  protected void setFilePath(String filePath)
  {
    this.filePath = filePath;
  }

  public boolean isLoadFromFile()
  {
    return this.loadFromFile;
  }

  public void setLoadFromFile(boolean loadFromFile)
  {
    this.loadFromFile = loadFromFile;
  }
}
