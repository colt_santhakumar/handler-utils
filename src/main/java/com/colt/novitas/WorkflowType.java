package com.colt.novitas;

public enum WorkflowType {
	UNDEFINED,
	CREATE_PORT,
	DELETE_PORT,
	CREATE_CONNECTION,
	MODIFY_CONNECTION,
	DELETE_CONNECTION,
	ONBOARDING;

}
