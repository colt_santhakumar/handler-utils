package com.colt.novitas.exception;


public enum HandlerErrorCode {

	//Create Port related
	GENERAL_ERROR_CODE(100, "Unable to find valid reason for fail", true, false),
	CP_INVALID_REQUEST(101, "Unable to find valid 'Create Port' request", false, false),
	CP_UPADTE_REQUEST_INPROGRESS(102, "Unable to update 'Create Port' request to INPROGRESS", false, false),
	CP_INVALID_SITE(103, "Cannot find site with location ID ", true, false),
	CP_CREATE_SERVICE_PORT(104, "Unable to create service port for request", true, false),
	CP_NO_MATCHING_RESOURCE_PORT(105, "Unable to retrieve a list of resource ports that match this request", true, false),
	CP_UPADTE_RESOURCE_PORT_ALLOCATED(106, "Unable to set resource port to state ALLOCATED", true, false),
	CP_UPDATE_SERVICE_PORT_DETAILS(107, "Unable to update service port with resource port details from port id", true, false),
	CP_PORT_INSTALL_CHARGE(201, "Unable to create Installation charge", false, true),
	CP_PORT_RECURRING_CHARGE(202, "Unable to create recurring charge", false, true),

	//Delete port related
	DP_INVALID_REQUEST(108, "Unable to find valid 'Delete Port' request", false, false),
	DP_UPADTE_REQUEST_INPROGRESS(109, "Unable to update 'Delete Port' request to INPROGRESS", false, false),
	DP_NO_MATCHING_SERVICE_PORT(110, "Unable to find service port for this request", true, false),
	DP_UPDATE_SERVICE_PORT_DECOMMISSIONING(111, "Unable to update service port status to DECOMMISSIONING", true, false),
	DP_UPDATE_SERVICE_PORT_DECOMMISSIONED(112, "Unable to update service port status to DECOMMISSIONED", true, false),
	DP_UPADTE_RESOURCE_PORT_AVAILABLE(113, "Unable to update resource port status to AVAILABLE", true, false),
	DP_PORT_DECOMISSING_CHARGE(203, "Unable to create decommissioning charge", false, true),
	DP_PORT_PENALTY_CHARGE(204, "Unable to create penalty charge", false, true),
	DP_PORT_CANCEL_RECURRING_CHARGE(205, "Unable to cancel recurring charge", false, true),


	//Create Connection related
	CC_INVALID_REQUEST(114, "Unable to find valid 'Create Connection' request", false, false),
	CC_UPADTE_REQUEST_INPROGRESS(115, "Unable to update 'Create Connection' request status to INPROGRESS", false, false),
	CC_CREATE_SERVICE_CONN(116, "Unable to create service connection for this request", true, false),
	CC_ROUTINE_CONNECTION(117, "Unable to create an EVC on the network (Route)", true, false),
	CC_CREATE_RESOURCE_CONNECTION(118, "Unable to create resource connection in database", true, false),
	CC_UPDATE_RESOURCE_PROVISION(119, "Unable to update resource connection in database with state PROVISIONED", true, false),
	CC_UPDATE_SERVICE_ACTIVE_CONN_ID(120, "Unable to update service connection to ACTIVE with resource connection details from connection id", true, false),
	CC_PROVISION_CONNECTION(121, "Unable to Provision connection on the network", true, false),
	CC_GET_SERVICE_PORT(122, "Unable to retrieve service port", true, false),
	CC_GET_RESOURCE_PORT(123, "Unable to retrieve resource port", true, false),
	CC_INSTALLATION_CHARGE(206, "Unable to create installtion charge", false, true),
	CC_RECURRING_CHARGE(207, "Unable to create recurring charge", false, true),


	//Modify connection
	MC_INVALID_REQUEST(124, "Unable to find valid 'Modify Connection' request", false, false),
	MC_UPADTE_REQUEST_INPROGRESS(125, "Unable to update 'Modify Connection' request status to INPROGRESS", false, false),
	MC_GET_SERVICE_CONNECTION(126, "Retrieve service connection failed", true, false),
	MC_GET_RESOURCE_CONNECTION(127, "Retrieve resource connection failed", true, false),
	MC_UPDATE_SERVICE_MODIFYING(128, "Unable to update service connection to MODIFYING", true, false),
	MC_MODIFY_CONNECTION(129, "Unable to modify connection on the network", true, false),
	MC_UPDATE_SERVICE_CONNECTION_ACTIVE(130, "Unable to update service connection to ACTIVE", true, false),
	MC_MODIFY_CONNECTION_CHARGE(208, "Unable to create modify connection charge", false, true),
	MC_MODIFY_RECURRING_CHARGE(209, "Unable to modify recurring charge", false, true),

	//Delete connection
	DC_INVALID_REQUEST(131, "Unable to find valid 'Delete Connection' request", false, false),
	DC_UPADTE_REQUEST_INPROGRESS(132, "Unable to update 'Delete Connection' request status to INPROGRESS", false, false),
	DC_GET_SERVICE_CONNECTION(133, "Retrieve service connection failed", true, false),
	DC_GET_RESOURCE_CONNECTION(134, "Retrieve resource connection failed", true, false),
	DC_UPDATE_SERVICE_DECOMMISSIONING(135, "Unable to update service connection to DECOMMISSIONING", true, false),
	DC_DEPROVISION_CONNECTION(136, "Unable to deprovision connection on the network", true, false),
	DC_UPDATE_RESOURCE_ROUTED(137, "Unable to update resource connection in database with state ROUTED", true, false),
	DC_DELETE_CONNECTION(138, "Unable to delete connection on the network", true, false),
	CEASE_RESOURCE_CONNECTION(139, "Unable to cease resource connection", true, false),
	DC_UPDATE_SERVICE_DECOMMISSIONED(140, "Unable to update service connection to DECOMMISSIONED", true, false),
	DC_END_RECURRING_CHARGE(210, "Unable to end recurring charge", false, true),

	//Register Callback details

	RG_CB_DETAILS(211, "Unable to save callback details", true, false),
	UPDATE_STATUS_CB_DETAILS(212, "Unable to update callback details", true, false),

	// Siebel Assets Mgmt
	CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT(213, "Unable to create or update Siebel Assets mgmt system", true, false),

	//Smart System
    NOTIFY_SMARTS_SYSTEM(214, "Unable to notify Smarts system", true, false),

	NOTIFY_ONBOARDING_KENAN_SYSTEM(215, "Unable to notify onboarding Kenan system", true, false),
	NOTIFY_ONBOARDING_SIEBEL_SYSTEM(216, "Unable to notify onboarding Siebel system", true, false),
	UPDATE_ONBOARDING_SERVICE_STATUS(217, "Unable to update onboarding service status", true, false),
	UPDATE_ONBOARDING_CUSTOMER_STATUS(218, "Unable to update onboarding customer status", true, false),
	FAILED_TO_TRANSLATE_SITE_TYPE(219, "Unable to translate composite sitetype to novitas site type", true, false),
	GET_CREATED_CONN_CIRCUITID(220,"Failed to get created connection circuit id",true,false),
	NO_PORTS_AVAILABLE(221,"No Ports available",true,false),
	FAILED_TO_RESERVE_AVAILABLE(222,"No Ports available",true,false),
	FAILED_TO_RECREATE_EVC_ID(223,"Failed to recreate evc id",true,false),
	CHECKING_PORT_NAME_EXISTANCE_IN_BPO(224,"Failed to check Portname existance in BPO",true,false),
	FAILED_TO_GET_CONFIG_PROPERTIES(225,"Failed to get Config Properties",true,false),
	FAILED_TO_GET_AZURE_DETAILS(226,"Failed to get Azure Port Details",true,false),
	NO_MATCHING_AZURE_VLAN_INDENTIFIERS_IN_XNG(227,"No Matching Azure VLAN Identifiers in XNG",true,false),

	// Callback errors
	RESERVE_PORT_CALL_BACK_STATUS(228,"Reserve Port callback status", true, false),
	PORT_SIEBEL_ASSETS_CALL_BACK_STATUS(229,"Siebel Assets callback status", true, false),

	CREATE_CONNECTION_CALL_BACK_STATUS(230,"Created Connection callback status failed", true, false),
	CONNECTION_SIEBEL_ASSETS_CALL_BACK_STATUS(231,"Connection Siebel Assets callback status failed", true, false),
	CONNECTION_SMARTS_CALL_BACK_STATUS(232,"Connection Smarts callback status failed", false, true),

	INVALID_PORT_STATUS(233,"Invalid Port Status to create connection",true,false),
	INVALID_AZURE_SERVICE_STATUS(234,"Invalid Azure Service Status to create connection",true,false),

	//SOFT HARD cease/restore
	NOVITAS_SERVICE_ERROR_DETAILS(235,"Failed to update Novitas Service with error code details",true,false),
	SUBPROCESS_STATUS_CHECK_FAILED(236,"Failed to get status of subprocess",true,false),
	SP_GET_REQUEST(237, "Unable to get 'SitePort' request", false, false),
	SP_UPADTE_REQUEST(238, "Unable to update 'SitePort' request status to INPROGRESS", false, false),
	SP_UPADTE_REQUEST_XTRAC(239, "Unable to update 'SitePort' request with XTRAC details", true, false),
	SP_INSTALL_CHARGE(240, "Unable to create Installation charge", false, true),
	SP_CAPACTIY_ORDER(241, "Capacity Order Failed in XTRAC", true, false),
	SP_CREAT_PORT_REQUEST(242,"Failed to Create Port request for Ordered Capacity", true, false),
	CP_AWSH_CREATED_PORT(243,"Failed to Create connection in AWS ", true, false),
	CP_AWSH_DELET_PORT(244,"Failed to Delete connection in AWS ", true, false),
	CP_AWSH_GET_CONNECTION(245,"Failed to Get AWS Connection Details", true, false),
	AWSH_CONNECTION_INVALID_STATUS(246,"Invalid AWS Hosted Connection Status", true, false),
	AWSH_GET_AVAILABLE_CAPACITY(247,"Failed to get Available Capacity", true, false),
	AWSH_RESERVE_VLAN(248,"Failed to Reserve VLAN", true, false),
	AWSH_RELEASE_VLAN(249,"Failed to Release VLAN", true, false),
	DC_CONNECTION_PENALTY_CHARGE(250, "Unable to create penalty charge", false, true),
	FAILED_TO_GET_CLOUD_SITE(251, "Unable to get Cloud Site Details", true, false),
	FAILED_TO_GET_DEDICATED_NNI_CIRCUIT(252, "Unable to get NNI Circuit details", true, false),
	LOA_NOT_ACCEPTED(253, "Faild to get AWS Ref from Admin", true, false),
	DEDICATED_PORT_PO_ORDER_REJECTED(254, "XTRAC sytem failed to route the circuit", true, false),
	FAILED_TO_GET_EVENTS_FOR_CONNECTION(255, "Failed to get schedule events for connection", true, false),
	INVALID_VLAN_FORMAT(256, "Invalid Vlan id", true, false),


	//MDSO
	MDSO_AUTHENTICATION_FAILED(257, "MDSO Authentication Failed", true, false),
	MDSO_SUSPEND_FAILED(258, "Soft ceasing connection failed in MDSO", true, false),
	MDSO_RESUNE_FAILED(259, "Soft restore connection failed in MDSO", true, false),
	INVALID_NW_TARGET(260, "Invalid Port Regions choosed", true, false),
	RETRIVE_NW_CONN_FAILED(261,"Failed to get EVC status",true,false),
	VALIDATED_SITE_NOT_FOUND(262,"Validated Site not found",true,false),
	FAILED_TO_GET_TEMP_LOCATION(263,"Failed top get Temp Location",true,false),
	FAILED_TO_UPDATE_TEMP_LOCATION(264,"Failed top get Temp Location",true,false),
	FAILED_TO_GET_LOCATION_ADDRESS_BOOK(265,"Failed top get Location from Addressbook ",true,false),
	FAILED_TO_UPDATE_ADDRESS_BOOK(266,"Failed to Add Location in Address Book",true,false),
	PORT_COMMUNICATION_STATE(267,"Failed to get Port Communication State",true,false),
	VERIZON_MC_FAILED (268,"Failed to update Verizon circuit",true,false),
	GET_CUSTOMER_FAILED(269,"Failed to get Customer Details",true,false),
	MS_SERVICE_KEY_INVALID_REGION(270,"MS Service Key region is not matching with Customer Region",true,false),
	MODIFY_RESOURCE_CONNECTION(271, "Unable to update resource connection in xng", true, false),

	NC_TECH_CREATE_PORT_ORDER(272, "NetCracker create port technical order failed", true, false),
	NC_TECH_CREATE_CONNECTION_ORDER(273, "NetCracker create connection technical order failed", true, false),
	NC_TECH_ORDER_CALLBACK(274, "NetCracker technical order callback failed", true, false),
	NC_TECH_DELETE_PORT_ORDER(275, "NetCracker technical order callback failed", true, false),
	NC_TECH_DELETE_CONNECTION_ORDER(276, "NetCracker technical order callback failed", true, false),
	NC_HANDOVER_DETAILS(277, "Failed to get NetCracker Handover details", true, false),
	OHS_NC_VALUES_MISSED(278, "Required values missed from OHS for NC capacity", true, false),
	NC_MSP_LABEL_DETAILS(279, "Failed to get MSP Port label details", true, false),

	// X Connect
	XC_GET_REQUEST(280, "Failed to get Cross connect request details", false, false),
	XC_UPADTE_REQUEST(281, "Failed to update Cross connect request details", false, false),
	XC_CREATE_SERVICE(282, "Failed to create Cross connect service", true, false),
	XC_UPADTE_SERVICE_AVAILABLE(283, "Failed to create Cross connect service", true, false),
	XC_INSTALL_CHARGE(284, "Unable to create Installation charge", false, true),
	XC_GET_SERVICE_DETAILS(285, "Failed to get cross connect servie details", true, false),
	XC_RECURRING_CHARGE(286, "Unable to create recurring charge", false, true),
	XC_DECOMMISSION_CHARGE(287, "Failed to apply decommission charges", false, true),
	XC_PENALTY_CHARGE(289, "Failed to apply penalty charges", false, true),
	CREATE_CEASE_REQUEST_ID(290, "Failed to create cease request id", true, false),
	UPDATE_CEASE_REQUEST_ID(291, "Failed to update cease request id", true, false),
	GCP_CREATE_INTERCONNECT_ATTACHMENT_FAILED(292, "Failed to create Interconnect Attachment", true, false),
	GCP_DELETE_INTERCONNECT_ATTACHMENT_FAILED(293, "Failed to delete Interconnect Attachment", true, false),
	GCP_INTERCONNECT_ATTACHMENT_INVALID_STATUS(295, "Interconnect Attachment invalid status", true, false),
	GCP_GET_INTERCONNECT_ATTACHMENT_FAILED(294, "Failed to get Interconnect Attachment", true, false),

	IBM_INTERCONNECT_ATTACHMENT_INVALID_STATUS(295, "Interconnect Attachment invalid status", true, false),
	IBM_GET_INTERCONNECT_ATTACHMENT_FAILED(294, "Failed to get Interconnect Attachment", true, false),

	// IP Access
	IPA_GET_REQUEST(294, "Unable to get IPA Connection request", false, false),
	IPA_GET_SERVICE_PORT(295, "Unable to get Service port for IPA Connection request", true, false),
	IPA_CREATE_SERVICE_CONN(296, "Unable to create service connection for this request", true, false),
	IPA_NO_SAR_NNI(297, "No available internet NNI for the site type and BW", true, false),
	IPA_ALLOCATE_SUBNET_FAILED(298, "Unable to allocate Subnet in EIP",true, false),
	IPA_RESERVE_IP_FAILED(299, "Unable to reserve IP in EIP",true, false),
	IPA_JUNIPER_SAR_REQ_FAILED(300, "Unable to config Juiper SAR",true, false),
	IPA_JUNIPER_SAR_MISSING_FIELD(301, "One or more fields are missing in request",true, false),
	IPA_SAR_FREE_NNI_BANDWIDTH_FAILED(302, "Unable to update free nni bandwidth ",true, false),
	IPA_CON_UPADTE_REQUEST(303, "Unable to update IPA Connection request status to INPROGRESS", false, false),
	IPA_RELEASE_IP_FAILED(304, "Unable to release IP in EIP",false, false),
	IPA_RELEASE_SUBNET_FAILED(305, "Unable to release Subnet in EIP",false, false),
	IPA_UPDATE_SERVICE_ACTIVE_CONN_ID(306, "Unable to update IPA service connection to ACTIVE with resource connection details from connection id", true, false),
	IPA_SAR_PORT_MISMATCH(307, "IPA SAR port and NC SAR port is not matching ", true, false),

	//Delete IPA connection
	IPA_DC_INVALID_REQUEST(308, "Unable to find valid 'Delete IPA Connection' request", false, false),
	IPA_DC_UPADTE_REQUEST_INPROGRESS(309, "Unable to update 'Delete IPA Connection' request status to INPROGRESS", false, false),
	IPA_DC_GET_SERVICE_CONNECTION(310, "IPA Retrieve service connection failed", true, false),
	IPA_DC_GET_RESOURCE_CONNECTION(311, "IPA Retrieve resource connection failed", true, false),
	IPA_DC_UPDATE_SERVICE_DECOMMISSIONING(312, "Unable to update IPA service connection to DECOMMISSIONING", true, false),
	IPA_DEPROVISION_CONNECTION(314, "Unable to deprovision IPA connection on the network", true, false),
	IPA_DC_UPDATE_RESOURCE_ROUTED(315, "Unable to update resource IPA connection in database with state ROUTED", true, false),
	IPA_DC_DELETE_CONNECTION(316, "Unable to delete IPA connection on the network", true, false),
	IPA_CEASE_RESOURCE_CONNECTION(317, "Unable to cease resource IPA connection", true, false),
	IPA_DC_UPDATE_SERVICE_DECOMMISSIONED(318, "Unable to update IPA service connection to DECOMMISSIONED", true, false),
	IPA_DC_END_RECURRING_CHARGE(319, "Unable to end recurring charge for IPA", false, true),
	IPA_RESERVE_VLAN_XNG(320, "Unable to reserve VLAN in XNG for IPA", false, true),
	IPA_RELEASE_VLAN_XNG(321, "Unable to release VLAN in XNG for IPA", false, true),

	//Spark

	SIEBEL_ORDER_CREATION_FAILED(322, "Spark: Siebel order creation failed", false, true),

	ORACLE_INTERCONNECT_ATTACHMENT_INVALID_STATUS(323, "Oracle Interconnect Attachment invalid status", true, false),
	ORACLE_GET_VIRTUAL_CIRCUIT_FAILED(324, "Failed to get Oracle Virtual Circuit details", true, false),
	ORACLE_UPDATE_VIRTUAL_CIRCUIT_FAILED(325, "Failed to update Oracle Virtual Circuit details", true, false),
	ORACLE_NO_BUILDING_FOUND_FOR_BANDWIDTH(326, "No building found for requested bandwidth", true, false),

	// Ticketing
	GET_USER_FAILED(327,"Failed to get User Details",true,false),
	SIEBEL_TICKET_CREATION_FAILED(328,"Failed to get User Details",true,false),

	EQUINIX_GET_INTERCONNECT_FAILED(329, "Failed to get Equinix Interconnect details", true, false),
	EQUINIX_UPDATE_INTERCONNECT_FAILED(330, "Failed to update Equinix Interconnect details", true, false),
	EQUINIX_NO_BUILDING_FOUND_FOR_BANDWIDTH(331, "No building found for requested bandwidth", true, false);

	private final Integer errorId;
	private final String description;
	private final boolean failRequest;
	private final boolean paymentError;

	HandlerErrorCode(Integer errorId, String description, boolean failRequest, boolean paymentError) {
		this.errorId = errorId;
		this.description = description;
		this.failRequest = failRequest;
		this.paymentError = paymentError;
	}

	public Integer getErrorId() {
		return errorId;
	}

	public String getDescription() {
		return description;
	}

	public boolean isFailRequest() {
		return failRequest;
	}

	public boolean isPaymentError() {
		return paymentError;
	}

}
