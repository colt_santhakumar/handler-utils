package com.colt.novitas.exception;

/**
 * Runtime exception for catching handler errors
 * @author YSoysa
 *
 */
public class NovitasHandlerException extends RuntimeException {

	private static final long serialVersionUID = -8831835934446585485L;
	
	private HandlerErrorCode errorCode;
	
	
	public NovitasHandlerException() {
	}

	/**
	 * 
	 * @param errorCode
	 */
	public NovitasHandlerException(HandlerErrorCode errorCode) {
		super(" Handler Error code : " + errorCode);
		this.errorCode = errorCode;
	}

	/**
	 * 
	 * @param errorCode
	 * @param message
	 */
	public NovitasHandlerException(HandlerErrorCode errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}
	
	/**
	 * 
	 * @param errorCode
	 * @param message
	 * @param throwable
	 */
	public NovitasHandlerException(HandlerErrorCode errorCode, String message, Throwable throwable) {
		super(message, throwable);
		this.errorCode = errorCode;
	}

	public HandlerErrorCode getErrorCode() {
		return errorCode;
	}
	
	public Integer getErrorCodeId() {
		return errorCode.getErrorId();
	}
	
	public String getErrorDescription() {
		return errorCode.getDescription();
	}
	
}
