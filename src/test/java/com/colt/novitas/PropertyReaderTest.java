package com.colt.novitas;

import com.colt.novitas.resource.client.ResourceAPIClient;
import com.colt.novitas.test.category.UnitTest;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import mockit.internal.MissingInvocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

@Category(UnitTest.class)
public class PropertyReaderTest {

    @Tested(availableDuringSetup = true) PropertyReader reader;

    @Mocked ResourceAPIClient client;

    @Before
    public void setUp() throws Exception {
        URL propsFile = PropertyReader.class.getResource("/test.properties");
        String filename = propsFile.getFile();
        System.out.println(filename);
        reader.setFilePath(filename);
    }

    @Test
    public void testGetDefaultPropertiesFromService() {

        new Expectations() {{
            client.getPropertiesAsMap("QA", (ArrayList) any); times = 0;
        }};

        EnvironmentProperties environmentProp = reader.getDefaultProperties();
        assertNotNull(environmentProp);
    }

    @Test
    public void testGetDefaultPropertiesFromFile() {
        reader.setLoadFromFile(true);

        new Expectations() {{
            client.getPropertiesAsMap("QA", (ArrayList) any); times = 0;
        }};

        EnvironmentProperties environmentProp = reader.getDefaultProperties();
        assertNotNull(environmentProp);

    }
}
